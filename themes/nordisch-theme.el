;;; nordisch-theme.el ---  A bluish clean theme, inspired by the Arctic Ice Studio Nord theme

;; Based on nord-emacs: https://github.com/arcticicestudio/nord-emacs

;; Title: Nordisch Theme
;; Project: nordisch-emacs
;; Version: 1.0
;; Package-Requires: ((emacs "24"))
;; License: MIT

;;; Commentary:

;;; References:
;; Emacs configuration
;;   https://gitlab.com/Vollpixel/emacs.d
;; Arctic Ice Studio:
;;   https://github.com/arcticicestudio
;; Nord-emacs:
;;   https://github.com/arcticicestudio/nord-emacs
;; Color Mixer:
;;   https://www.w3schools.com/colors/colors_mixer.asp

;;; Code:

(unless (>= emacs-major-version 24)
  (error "Nordisch theme requires Emacs 24 or later!"))

(deftheme nordisch "A nordisch-bluish clean theme")

(defgroup nordisch nil
  "Nordisch theme customizations.
  The theme has to be reloaded after changing anything in this group."
  :group 'faces)

(defcustom nordisch-region-highlight nil
  "Allows to set a region highlight style based on the Nordisch components.
  Valid styles are
    - 'snowstorm' - Uses 'nordisch0' as foreground- and 'nordisch4' as background color
    - 'frost' - Uses 'nordisch0' as foreground- and 'nordisch8' as background color"
  :type 'string
  :group 'nordisch)

(defcustom nordisch-uniform-mode-lines nil
  "Enables uniform activate- and inactive mode lines using 'nordisch3' as background."
  :type 'boolean
  :group 'nordisch)

;; https://colordesigner.io/gradient-generator
(setq nordisch-theme-bg-fg-gradient '("#282d39" "#313642" "#3a3f4a" "#434853" "#4c515c"
                                      "#565b66" "#60646f" "#696e78" "#747882" "#7e828c"
                                      "#888d96" "#9397a0" "#9da2aa" "#a8acb4" "#b3b7bf"
                                      "#bec2c9" "#cacdd4" "#d5d8de" "#e0e4e9" "#eceff4"))

(setq nordisch-theme--brightened-comments '("#4c566a" "#4e586d" "#505b70" "#525d73" "#556076"
                                            "#576279" "#59647c" "#5b677f" "#5d6982" "#5f6c85"
                                            "#616e88" "#63718b" "#66738e" "#687591" "#6a7894"
                                            "#6d7a96" "#6f7d98" "#72809a" "#75829c" "#78859e"
                                            "#7b88a1" "#808da6" "#8592ac" "#8b98b1" "#909db7"
                                            "#95a2bc" "#9aa8c2" "#a0adc7" "#a5b2cd" "#aab9d2"))

(setq nordisch-green-to-bg '("#a3be8c" "#9db788" "#97b084" "#91a880" "#8aa17b" "#849a77" "#7e9273"
                             "#788b6f" "#72846b" "#6c7d67" "#667662" "#5f6e5e" "#59675a" "#536056"
                             "#4d5852" "#47514e" "#414a4a" "#3a4345" "#343c41" "#2e343d" "#282d39"))

(setq nordisch-yellow-to-bg '("#ebcb8b" "#e1c387" "#d8bb83" "#ceb37f" "#c4ab7b" "#baa476" "#b09c72"
                              "#a7946e" "#9d8c6a" "#938466" "#8a7c62" "#80745e" "#766c5a" "#6c6456"
                              "#635c52" "#59544e" "#4f4d49" "#454545" "#3c3d41" "#32353d" "#282d39"))

(setq nordisch-red-to-bg '("#bf616a" "#b75e68" "#b05c65" "#a85963" "#a15760" "#99545e" "#92515b"
                           "#8a4f59" "#834c56" "#7b4a54" "#744752" "#6c444f" "#64424d" "#5d3f4a"
                           "#553d48" "#4e3a45" "#463743" "#3f3540" "#37323e" "#30303b" "#282d39"))

(defun nordisch-display-truecolor-or-graphic-p ()
  "Returns whether the display can display nordisch colors"
  (or (= (display-color-cells) 16777216) (display-graphic-p)))

;; Color Mixer: https://www.w3schools.com/colors/colors_mixer.asp

;;;; Color Constants
(let ((class '((class color) (min-colors 89)))
      (nordisch0 (if (nordisch-display-truecolor-or-graphic-p) "#282D39" nil))
      (nordisch1 (if (nordisch-display-truecolor-or-graphic-p) "#3B4252" "black"))
      (nordisch2 (if (nordisch-display-truecolor-or-graphic-p) "#434C5E" "#434C5E"))
      (nordisch3 (if (nordisch-display-truecolor-or-graphic-p) "#4C566A" "brightblack"))
      (nordisch4 (if (nordisch-display-truecolor-or-graphic-p) "#D8DEE9" "#D8DEE9"))
      (nordisch5 (if (nordisch-display-truecolor-or-graphic-p) "#E5E9F0" "white"))
      (nordisch6 (if (nordisch-display-truecolor-or-graphic-p) "#ECEFF4" "brightwhite"))
      (nordisch7 (if (nordisch-display-truecolor-or-graphic-p) "#8FBCBB" "cyan"))
      (nordisch8 (if (nordisch-display-truecolor-or-graphic-p) "#88C0D0" "brightcyan"))
      (nordisch9 (if (nordisch-display-truecolor-or-graphic-p) "#81A1C1" "brightblue"))
      (nordisch10 (if (nordisch-display-truecolor-or-graphic-p) "#5E81AC" "brightblue"))
      (nordisch11 (if (nordisch-display-truecolor-or-graphic-p) "#BF616A" "OrangeRed"))
      (nordisch12 (if (nordisch-display-truecolor-or-graphic-p) "#D08770" "DarkOrange"))
      (nordisch13 (if (nordisch-display-truecolor-or-graphic-p) "#EBCB8B" "gold"))
      (nordisch14 (if (nordisch-display-truecolor-or-graphic-p) "#A3BE8C" "green"))
      (nordisch15 (if (nordisch-display-truecolor-or-graphic-p) "#B48EAD" "magenta"))
      (nordisch-wt (if (nordisch-display-truecolor-or-graphic-p) "white" "white"))
      (nordisch11-dark (if (nordisch-display-truecolor-or-graphic-p) "#994e55" "OrangeRed"))
      (nordisch13-dark (if (nordisch-display-truecolor-or-graphic-p) "#bda370" "tan"))
      (nordisch13-dark-1 (if (nordisch-display-truecolor-or-graphic-p) "#a7946e" "tan"))
      (nordisch13-dark-2 (if (nordisch-display-truecolor-or-graphic-p) "#938466" "tan"))
      (nordisch14-dark (if (nordisch-display-truecolor-or-graphic-p) "#849a72" "OliveDrab"))
      (nordisch14-dark-1 (if (nordisch-display-truecolor-or-graphic-p) "#788b6f" "OliveDrab"))
      (nordisch14-dark-2 (if (nordisch-display-truecolor-or-graphic-p) "#6c7d67" "DarkOliveGreen"))
      (nordisch7-bright (if (nordisch-display-truecolor-or-graphic-p) "#afdddc" "cyan"))
      (nordisch9-bright (if (nordisch-display-truecolor-or-graphic-p) "#a1c1e3" "blue"))
      (nordisch11-bright (if (nordisch-display-truecolor-or-graphic-p) "#e28088" "OrangeRed"))
      (nordisch13-bright (if (nordisch-display-truecolor-or-graphic-p) "#ffe7a6" "gold"))
      (nordisch14-bright (if (nordisch-display-truecolor-or-graphic-p) "#c4e0ac" "green"))
      (nordisch15-bright (if (nordisch-display-truecolor-or-graphic-p) "#d5aece" "magenta"))
      (nordisch-light-7 (if (nordisch-display-truecolor-or-graphic-p) "#556076" nil))
      (nordisch-light-6 (if (nordisch-display-truecolor-or-graphic-p) "#444a58" nil))
      (nordisch-light-5 (if (nordisch-display-truecolor-or-graphic-p) "#3C414E" nil))
      (nordisch-light-4 (if (nordisch-display-truecolor-or-graphic-p) "#393E4C" nil))
      (nordisch-light-3 (if (nordisch-display-truecolor-or-graphic-p) "#353A47" nil))
      (nordisch-light-2 (if (nordisch-display-truecolor-or-graphic-p) "#303543" nil))
      (nordisch-light-1 (if (nordisch-display-truecolor-or-graphic-p) "#2C313E" nil))
      (nordisch-dark-1 (if (nordisch-display-truecolor-or-graphic-p) "#242935" nil))
      (nordisch-dark-1.5 (if (nordisch-display-truecolor-or-graphic-p) "#222733" nil))
      (nordisch-dark-2 (if (nordisch-display-truecolor-or-graphic-p) "#202530" nil))
      (nordisch-dark-3 (if (nordisch-display-truecolor-or-graphic-p) "#1C212C" nil))
      (nordisch-dark-4 (if (nordisch-display-truecolor-or-graphic-p) "#181D28" nil))
      (nordisch-dark-5 (if (nordisch-display-truecolor-or-graphic-p) "#141924" nil))
      (nordisch-match (if (nordisch-display-truecolor-or-graphic-p) "#6c8cab" nil))
      (nordisch-match-dark (if (nordisch-display-truecolor-or-graphic-p) "#436481" nil))
      (nordisch-annotation (if (nordisch-display-truecolor-or-graphic-p) "#D08770" "brightyellow"))
      (nordisch-attribute (if (nordisch-display-truecolor-or-graphic-p) "#8FBCBB" "cyan"))
      (nordisch-class (if (nordisch-display-truecolor-or-graphic-p) "#8FBCBB" "cyan"))
      (nordisch-comment (if (nordisch-display-truecolor-or-graphic-p)
                            (nth 20 nordisch-theme--brightened-comments) "brightblack"))
      (nordisch-linum (if (nordisch-display-truecolor-or-graphic-p)
                          (nth 7 nordisch-theme--brightened-comments) "brightblack"))
      (nordisch-linum-hl (if (nordisch-display-truecolor-or-graphic-p)
                             (nth 26 nordisch-theme--brightened-comments) "white"))
      (nordisch-hl-bg (if (nordisch-display-truecolor-or-graphic-p)
                          (nth 1 nordisch-theme-bg-fg-gradient) "black"))
      (nordisch-gg-new (if (nordisch-display-truecolor-or-graphic-p)
                           (nth 6 nordisch-green-to-bg) "DarkOliveGreen"))
      (nordisch-gg-changed (if (nordisch-display-truecolor-or-graphic-p)
                               (nth 8 nordisch-yellow-to-bg) "gold"))
      (nordisch-gg-deleted (if (nordisch-display-truecolor-or-graphic-p)
                               (nth 6 nordisch-red-to-bg) "DarkRed"))
      (nordisch-variable (if (nordisch-display-truecolor-or-graphic-p) "#CBC0A9" "#CBC0A9"))
      ;;(nordisch-constant (if (nordisch-display-truecolor-or-graphic-p) "#EBCB8B" "brightyellow"))
      (nordisch-constant (if (nordisch-display-truecolor-or-graphic-p) "#DDA97D" "brightyellow"))
      (nordisch-cursor (if (nordisch-display-truecolor-or-graphic-p) "#ffe7a6" "gold"))
      (nordisch-escape (if (nordisch-display-truecolor-or-graphic-p) "#D08770" "brightyellow"))
      (nordisch-method (if (nordisch-display-truecolor-or-graphic-p) "#88C0D0" "brightcyan"))
      (nordisch-keyword (if (nordisch-display-truecolor-or-graphic-p) "#81A1C1" "blue"))
      (nordisch-numeric (if (nordisch-display-truecolor-or-graphic-p) "#B48EAD" "magenta"))
      (nordisch-operator (if (nordisch-display-truecolor-or-graphic-p) "#81A1C1" "blue"))
      (nordisch-preprocessor (if (nordisch-display-truecolor-or-graphic-p) "#5E81AC" "brightblue"))
      (nordisch-punctuation (if (nordisch-display-truecolor-or-graphic-p) "#D8DEE9" "#D8DEE9"))
      (nordisch-regexp (if (nordisch-display-truecolor-or-graphic-p) "#EBCB8B" "gold"))
      (nordisch-string (if (nordisch-display-truecolor-or-graphic-p) "#A3BE8C" "green"))
      (nordisch-tag (if (nordisch-display-truecolor-or-graphic-p) "#81A1C1" "blue"))
      (nordisch-icy (if (nordisch-display-truecolor-or-graphic-p) "#C0CCF3" "#C6CCF1"))
      (nordisch-pure-ylw (if (nordisch-display-truecolor-or-graphic-p) "gold" "yellow"))
      (nordisch-darkred (if (nordisch-display-truecolor-or-graphic-p) "DarkRed" "DarkRed"))
      (nordisch-region-highlight-foreground (if (or (string= nordisch-region-highlight "frost")
                                                    (string= nordisch-region-highlight "snowstorm")) "#2E3440" "#D8DEE9"))
      (nordisch-region-highlight-background (if (string= nordisch-region-highlight "frost") "#88C0D0"
                                              (if (string= nordisch-region-highlight "snowstorm") "#D8DEE9" "#434C5E")))
      (nordisch-uniform-mode-lines-background (if nordisch-uniform-mode-lines "#4C566A" "#3B4252")))

  ;; Faces
  ;;;;;;;;;;

  (custom-theme-set-faces
   'nordisch
   ;; Base
   `(default ((,class (:foreground ,nordisch4 :background ,nordisch0))))
   `(italic ((,class (:slant italic))))
   `(bold ((,class (:weight bold))))
   `(bold-italic ((,class (:weight bold :slant italic))))
   `(underline ((,class (:underline t))))
   `(warning ((,class (:foreground ,nordisch13 :weight bold))))
   `(error ((,class (:foreground ,nordisch11-bright :weight bold))))
   `(shadow ((,class (:foreground ,nordisch-comment))))
   `(escape-glyph ((,class (:foreground ,nordisch12))))
   `(font-lock-builtin-face ((,class (:foreground ,nordisch13))))
   `(font-lock-comment-face ((,class (:foreground ,nordisch-comment :slant italic))))
   `(font-lock-comment-delimiter-face ((,class (:foreground ,nordisch-comment))))
   `(font-lock-constant-face ((,class (:foreground ,nordisch-constant))))
   `(font-lock-doc-face ((,class (:foreground ,nordisch-comment))))
   `(font-lock-function-name-face ((,class (:foreground ,nordisch8))))
   `(font-lock-keyword-face ((,class (:foreground ,nordisch9))))
   `(font-lock-negation-char-face ((,class (:foreground ,nordisch9))))
   `(font-lock-preprocessor-face ((,class (:foreground ,nordisch10 :weight bold))))
   `(font-lock-reference-face ((,class (:foreground ,nordisch9))))
   `(font-lock-regexp-grouping-backslash ((,class (:foreground ,nordisch13))))
   `(font-lock-regexp-grouping-construct ((,class (:foreground ,nordisch13))))
   `(font-lock-string-face ((,class (:foreground ,nordisch14))))
   `(font-lock-type-face ((,class (:foreground ,nordisch7))))
   `(font-lock-variable-name-face ((,class (:foreground ,nordisch-variable))))
   `(font-lock-warning-face ((,class (:foreground ,nordisch13))))

   ;; Line numbers
   `(line-number ((,class (:foreground ,nordisch-linum))))
   `(line-number-current-line ((,class (:weight bold :foreground ,nordisch-linum-hl))))
   ;; `(line-number-current-line ((,class (:weight bold :background ,nordisch-dark-1 :foreground ,nordisch-linum-hl))))

   ;; UI
   `(border ((,class (:foreground ,nordisch4))))
   `(cursor ((,class (:background ,nordisch-cursor))))
   `(scroll-bar ((,class (:background ,nordisch0 :foreground ,nordisch1))))
   `(buffer-menu-buffer ((,class (:foreground ,nordisch4 :weight bold))))
   `(button ((,class (:background ,nordisch0 :foreground ,nordisch8 :box (:line-width 1 :color ,nordisch2)))))
   `(completions-annotations ((,class (:foreground ,nordisch9))))
   `(completions-common-part ((,class (:foreground ,nordisch8 :weight bold))))
   `(completions-first-difference ((,class (:foreground ,nordisch11))))
   `(custom-button ((,class (:background ,nordisch0 :foreground ,nordisch8 :box (:line-width 1 :color ,nordisch9)))))
   `(custom-button-mouse ((,class (:background ,nordisch4 :foreground ,nordisch0 :box (:line-width 2 :color ,nordisch4)))))
   `(custom-button-pressed ((,class (:background ,nordisch6 :foreground ,nordisch0 :box (:line-width 2 :color ,nordisch4)))))
   `(custom-button-pressed-unraised ((,class (:background ,nordisch4 :foreground ,nordisch0 :box (:line-width 2 :color ,nordisch4)))))
   `(custom-button-unraised ((,class (:background ,nordisch0 :foreground ,nordisch8 :box (:line-width 2 :color ,nordisch4)))))
   `(custom-changed ((,class (:foreground ,nordisch13))))
   `(custom-comment ((,class (:foreground ,nordisch-comment :slant italic))))
   `(custom-comment-tag ((,class (:foreground ,nordisch7))))
   `(custom-documentation ((,class (:foreground ,nordisch4))))
   `(custom-group-tag ((,class (:foreground ,nordisch8 :weight bold))))
   `(custom-group-tag-1 ((,class (:foreground ,nordisch8 :weight bold))))
   `(custom-invalid ((,class (:foreground ,nordisch11))))
   `(custom-modified ((,class (:foreground ,nordisch13))))
   `(custom-rogue ((,class (:foreground ,nordisch12 :background ,nordisch2))))
   `(custom-saved ((,class (:foreground ,nordisch14))))
   `(custom-set ((,class (:foreground ,nordisch8))))
   `(custom-state ((,class (:foreground ,nordisch14))))
   `(custom-themed ((,class (:foreground ,nordisch8 :background ,nordisch2))))
   `(fringe ((,class (:foreground ,nordisch4 :background ,nordisch0))))
   `(file-name-shadow ((,class (:inherit shadow))))
   `(header-line ((,class (:foreground ,nordisch4 :background ,nordisch2))))
   `(help-argument-name ((,class (:foreground ,nordisch8))))
   `(help-key-binding ((,class (:background ,nordisch-light-1 :foreground ,nordisch-variable :weight bold :box (:line-width 1 :color ,nordisch2)))))
   `(highlight ((,class (:foreground ,nordisch8 :background ,nordisch2))))
   `(hl-line ((,class (:background ,nordisch-hl-bg))))
   `(isearch ((,class (:foreground ,nordisch0 :background ,nordisch8))))
   `(isearch-fail ((,class (:foreground ,nordisch11))))
   `(link ((,class (:underline t))))
   `(link-visited ((,class (:underline t))))
   `(linum ((,class (:foreground ,nordisch3 :background ,nordisch0))))
   `(linum-relative-current-face ((,class (:foreground ,nordisch3 :background ,nordisch0))))
   `(match ((,class (:inherit isearch))))
   `(minibuffer-prompt ((,class (:foreground ,nordisch8 :weight bold))))
   `(mm-command-output ((,class (:foreground ,nordisch8))))
   `(mode-line ((,class (:foreground ,nordisch8 :background ,nordisch-dark-2))))
   `(mode-line-buffer-id ((,class (:weight bold))))
   `(mode-line-highlight ((,class (:inherit highlight))))
   `(mode-line-inactive ((,class (:foreground ,nordisch4 :background ,nordisch-dark-1))))
   `(next-error ((,class (:inherit error))))
   `(nobreak-space ((,class (:foreground ,nordisch3))))

   `(outline-1 ((,class (:foreground ,nordisch8 :weight bold))))
   `(outline-2 ((,class (:inherit outline-1))))
   `(outline-3 ((,class (:inherit outline-1))))
   `(outline-4 ((,class (:inherit outline-1))))
   `(outline-5 ((,class (:inherit outline-1))))
   `(outline-6 ((,class (:inherit outline-1))))
   `(outline-7 ((,class (:inherit outline-1))))
   `(outline-8 ((,class (:inherit outline-1))))
   `(package-description ((,class (:foreground ,nordisch4))))
   `(package-help-section-name ((,class (:foreground ,nordisch8 :weight bold))))
   `(package-name ((,class (:foreground ,nordisch8))))
   `(package-status-available ((,class (:foreground ,nordisch7))))
   `(package-status-avail-obso ((,class (:foreground ,nordisch7 :slant italic))))
   `(package-status-built-in ((,class (:foreground ,nordisch9))))
   `(package-status-dependency ((,class (:foreground ,nordisch8 :slant italic))))
   `(package-status-disabled ((,class (:foreground ,nordisch3))))
   `(package-status-external ((,class (:foreground ,nordisch12 :slant italic))))
   `(package-status-held ((,class (:foreground ,nordisch4 :weight bold))))
   `(package-status-new ((,class (:foreground ,nordisch14))))
   `(package-status-incompat ((,class (:foreground ,nordisch11))))
   `(package-status-installed ((,class (:foreground ,nordisch7 :weight bold))))
   `(package-status-unsigned ((,class (:underline ,nordisch13))))
   `(query-replace ((,class (:foreground ,nordisch8 :background ,nordisch2))))
   `(region ((,class (:foreground ,nordisch-region-highlight-foreground :background ,nordisch-region-highlight-background))))
   `(secondary-selection ((,class (:background ,nordisch-light-3))))
   `(highlight-indentation-face ((,class (:background ,nordisch-light-2))))

   ;; Mail
   `(message-cited-text ((,class (:foreground ,nordisch4))))
   `(message-header-name ((,class (:foreground ,nordisch7 :weight bold))))
   `(message-header-subject ((,class (:foreground ,nordisch8 :weight bold :height 1.1))))
   `(message-header-to ((,class (:foreground ,nordisch9))))
   `(message-header-cc ((,class (:foreground ,nordisch9))))
   `(message-header-other ((,class (:foreground ,nordisch4))))
   `(message-header-newsgroup ((,class (:foreground ,nordisch14))))
   `(message-header-xheader ((,class (:foreground ,nordisch13))))
   `(message-mml ((,class (:foreground ,nordisch10))))
   `(message-separator ((,class (:inherit shadow))))

   ;; show-paren-match-face and show-paren-mismatch-face are deprecated since Emacs
   ;; version 22.1 and were removed in Emacs 25.
   ;; https://github.com/arcticicestudio/nord-emacs/issues/75
   `(show-paren-match ((,class (:foreground ,nordisch6 :background ,nordisch-match-dark))))
   `(show-paren-mismatch ((,class (:background ,nordisch11))))
   `(sh-quoted-exec ((,class (:foreground ,nordisch-constant))))
   `(sh-heredoc ((,class (:foreground ,nordisch14-bright))))
   `(success ((,class (:foreground ,nordisch14))))
   `(term ((,class (:foreground ,nordisch4 :background ,nordisch0))))
   `(term-color-black ((,class (:foreground ,nordisch1 :background ,nordisch1))))
   `(term-color-white ((,class (:foreground ,nordisch5 :background ,nordisch5))))
   `(term-color-cyan ((,class (:foreground ,nordisch7 :background ,nordisch7))))
   `(term-color-blue ((,class (:foreground ,nordisch8 :background ,nordisch8))))
   `(term-color-red ((,class (:foreground ,nordisch11 :background ,nordisch11))))
   `(term-color-yellow ((,class (:foreground ,nordisch13 :background ,nordisch13))))
   `(term-color-green ((,class (:foreground ,nordisch14 :background ,nordisch14))))
   `(term-color-magenta ((,class (:foreground ,nordisch15 :background ,nordisch15))))
   `(tool-bar ((,class (:foreground ,nordisch4 :background ,nordisch3))))
   `(tooltip ((,class (:foreground ,nordisch0 :background ,nordisch4))))
   `(trailing-whitespace ((,class (:background ,nordisch-light-2))))
   `(tty-menu-disabled-face ((,class (:foreground ,nordisch1))))
   `(tty-menu-enabled-face ((,class (:background ,nordisch2 foreground ,nordisch4))))
   `(tty-menu-selected-face ((,class (:foreground ,nordisch8 :underline t))))
   `(undo-tree-visualizer-current-face ((,class (:foreground ,nordisch8))))
   `(undo-tree-visualizer-default-face ((,class (:foreground ,nordisch4))))
   `(undo-tree-visualizer-unmodified-face ((,class (:foreground ,nordisch4))))
   `(undo-tree-visualizer-register-face ((,class (:foreground ,nordisch9))))
   `(vc-conflict-state ((,class (:foreground ,nordisch12))))
   `(vc-edited-state ((,class (:foreground ,nordisch13))))
   `(vc-locally-added-state ((,class (:underline ,nordisch14))))
   `(vc-locked-state ((,class (:foreground ,nordisch10))))
   `(vc-missing-state ((,class (:foreground ,nordisch11))))
   `(vc-needs-update-state ((,class (:foreground ,nordisch12))))
   `(vc-removed-state ((,class (:foreground ,nordisch11))))
   `(vc-state-base ((,class (:foreground ,nordisch4))))
   `(vc-up-to-date-state ((,class (:foreground ,nordisch8))))
   `(vertical-border ((,class (:foreground ,nordisch2))))
   `(which-func ((,class (:foreground ,nordisch8))))
   `(whitespace-big-indent ((,class (:foreground ,nordisch3 :background ,nordisch0))))
   `(whitespace-empty ((,class (:foreground ,nordisch3 :background ,nordisch0))))
   `(whitespace-hspace ((,class (:foreground ,nordisch3 :background ,nordisch0))))
   `(whitespace-indentation ((,class (:foreground ,nordisch3 :background ,nordisch0))))
   `(whitespace-line ((,class (:background ,nordisch0))))
   `(whitespace-newline ((,class (:foreground ,nordisch3 :background ,nordisch0))))
   `(whitespace-space ((,class (:foreground ,nordisch3 :background ,nordisch0))))
   `(whitespace-space-after-tab ((,class (:foreground ,nordisch3 :background ,nordisch0))))
   `(whitespace-space-before-tab ((,class (:foreground ,nordisch3 :background ,nordisch0))))
   `(whitespace-tab ((,class (:foreground ,nordisch3 :background ,nordisch0))))
   `(whitespace-trailing ((,class (:inherit trailing-whitespace))))
   `(widget-button-pressed ((,class (:foreground ,nordisch9 :background ,nordisch1))))
   `(widget-documentation ((,class (:foreground ,nordisch4))))
   `(widget-field ((,class (:background ,nordisch2 :foreground ,nordisch4))))
   `(widget-single-line-field ((,class (:background ,nordisch2 :foreground ,nordisch4))))
   `(window-divider ((,class (:background ,nordisch3))))
   `(window-divider-first-pixel ((,class (:background ,nordisch3))))
   `(window-divider-last-pixel ((,class (:background ,nordisch3))))

   ;; ANSI colors
   `(ansi-color-red ((,class (:foreground ,nordisch11))))
   `(ansi-color-cyan ((,class (:foreground ,nordisch7))))
   `(ansi-color-blue ((,class (:foreground ,nordisch9))))
   `(ansi-color-green ((,class (:foreground ,nordisch14))))
   `(ansi-color-black ((,class (:foreground ,nordisch-dark-5))))
   `(ansi-color-white ((,class (:foreground ,nordisch5))))
   `(ansi-color-yellow ((,class (:foreground ,nordisch13))))
   `(ansi-color-magenta ((,class (:foreground ,nordisch15))))
   `(ansi-color-bright-red ((,class (:foreground ,nordisch11-bright))))
   `(ansi-color-bright-cyan ((,class (:foreground ,nordisch7-bright))))
   `(ansi-color-bright-blue ((,class (:foreground ,nordisch9-bright))))
   `(ansi-color-bright-black ((,class (:foreground ,nordisch-dark-3))))
   `(ansi-color-bright-green ((,class (:foreground ,nordisch14-bright))))
   `(ansi-color-bright-white ((,class (:foreground ,nordisch-wt))))
   `(ansi-color-bright-yellow ((,class (:foreground ,nordisch13-bright))))
   `(ansi-color-bright-magenta ((,class (:foreground ,nordisch15-bright))))

   ;; Info
   `(info-menu-star ((,class (:foreground ,nordisch4))))
   `(info-title-1 ((,class (:foreground ,nordisch7 :weight bold :height 1.4))))
   `(info-title-2 ((,class (:foreground ,nordisch8 :weight bold :height 1.3))))
   `(info-title-3 ((,class (:foreground ,nordisch9 :weight bold :height 1.2))))
   `(info-title-4 ((,class (:foreground ,nordisch10 :weight bold :height 1.1))))
   `(info-menu-header ((,class (:foreground ,nordisch-variable :weight bold :height 1.3))))
   `(info-quoted ((,class (:foreground ,nordisch7))))
   `(info-xref ((,class (:foreground ,nordisch8 :underline t))))

   ;; imenu-list
   `(imenu-list-entry-face ((,class (:foreground ,nordisch4))))
   `(imenu-list-entry-face-0 ((,class (:foreground ,nordisch4))))
   `(imenu-list-entry-face-1 ((,class (:foreground ,nordisch-variable))))
   `(imenu-list-entry-face-2 ((,class (:foreground ,nordisch12))))
   `(imenu-list-entry-face-3 ((,class (:foreground ,nordisch14))))
   `(imenu-list-entry-subalist-face-0 ((,class (:foreground ,nordisch8))))
   `(imenu-list-entry-subalist-face-1 ((,class (:foreground ,nordisch12))))
   `(imenu-list-entry-subalist-face-2 ((,class (:foreground ,nordisch14))))
   `(imenu-list-entry-subalist-face-3 ((,class (:foreground ,nordisch15))))

   ;; Smartparens
   `(sp-pair-overlay-face ((,class (:background ,nordisch-dark-3))))

   ;; Highlight Symbol
   `(highlight-symbol-face ((,class (:background ,nordisch-light-4 :underline ,nordisch9))))
   ;; `(highlight-symbol-face ((,class (:background ,nordisch-light-4))))

   ;; C
   `(c-annotation-face ((,class (:foreground ,nordisch-annotation))))

   ;; diff
   `(diff-added ((,class (:foreground ,nordisch14))))
   `(diff-changed ((,class (:foreground ,nordisch13))))
   `(diff-context ((,class (:inherit default))))
   `(diff-file-header ((,class (:foreground ,nordisch8))))
   `(diff-function ((,class (:foreground ,nordisch7))))
   `(diff-header ((,class (:foreground ,nordisch9 :weight bold))))
   `(diff-hunk-header ((,class (:foreground ,nordisch9 :background ,nordisch0))))
   `(diff-indicator-added ((,class (:foreground ,nordisch14))))
   `(diff-indicator-changed ((,class (:foreground ,nordisch13))))
   `(diff-indicator-removed ((,class (:foreground ,nordisch11))))
   `(diff-nonexistent ((,class (:foreground ,nordisch11))))
   `(diff-refine-added ((,class (:foreground ,nordisch14))))
   `(diff-refine-changed ((,class (:foreground ,nordisch13))))
   `(diff-refine-removed ((,class (:foreground ,nordisch11))))
   `(diff-removed ((,class (:foreground ,nordisch11))))

   ;; Auctex
   `(font-latex-bold-face ((,class (:inherit bold))))
   `(font-latex-italic-face ((,class (:inherit italic))))
   `(font-latex-math-face ((,class (:foreground ,nordisch8))))
   `(font-latex-sectioning-0-face ((,class (:foreground ,nordisch8 :weight bold))))
   `(font-latex-sectioning-1-face ((,class (:inherit font-latex-sectioning-0-face))))
   `(font-latex-sectioning-2-face ((,class (:inherit font-latex-sectioning-0-face))))
   `(font-latex-sectioning-3-face ((,class (:inherit font-latex-sectioning-0-face))))
   `(font-latex-sectioning-4-face ((,class (:inherit font-latex-sectioning-0-face))))
   `(font-latex-sectioning-5-face ((,class (:inherit font-latex-sectioning-0-face))))
   `(font-latex-script-char-face ((,class (:inherit font-lock-warning-face))))
   `(font-latex-string-face ((,class (:inherit font-lock-string-face))))
   `(font-latex-warning-face ((,class (:inherit font-lock-warning-face))))
   `(font-latex-bold-face ((,class (:inherit bold))))
   `(font-latex-italic-face ((,class (:slant italic))))
   `(font-latex-string-face ((,class (:foreground ,nordisch14))))
   `(font-latex-underline-face ((,class (:inherit underline :foreground ,nordisch14))))
   `(font-latex-match-reference-keywords ((,class (:foreground ,nordisch9))))
   `(font-latex-match-variable-keywords ((,class (:foreground ,nordisch4))))

   ;; Clojure
   `(clojure-keyword-face ((,class (:foreground ,nordisch13))))

   ;; Elixir
   `(elixir-attribute-face ((,class (:foreground ,nordisch-annotation))))
   `(elixir-atom-face ((,class (:foreground ,nordisch4 :weight bold))))

   ;; Enhanced Ruby
   `(enh-ruby-heredoc-delimiter-face ((,class (:foreground ,nordisch14))))
   `(enh-ruby-op-face ((,class (:foreground ,nordisch9))))
   `(enh-ruby-regexp-delimiter-face ((,class (:foreground ,nordisch13))))
   `(enh-ruby-regexp-face ((,class (:foreground ,nordisch13))))
   `(enh-ruby-string-delimiter-face ((,class (:foreground ,nordisch14))))
   `(erm-syn-errline ((,class (:foreground ,nordisch11 :underline t))))
   `(erm-syn-warnline ((,class (:foreground ,nordisch13 :underline t))))

   ;; Java Development Environment for Emacs
   `(jdee-db-active-breakpoint-face ((,class (:background ,nordisch2 :weight bold))))
   `(jdee-bug-breakpoint-cursor ((,class (:background ,nordisch2))))
   `(jdee-db-requested-breakpoint-face ((,class (:foreground ,nordisch13 :background ,nordisch2 :weight bold))))
   `(jdee-db-spec-breakpoint-face ((,class (:foreground ,nordisch14 :background ,nordisch2 :weight bold))))
   `(jdee-font-lock-api-face ((,class (:foreground ,nordisch4))))
   `(jdee-font-lock-code-face ((,class (:slant italic))))
   `(jdee-font-lock-constant-face ((,class (:foreground ,nordisch-keyword))))
   `(jdee-font-lock-constructor-face ((,class (:foreground ,nordisch-method))))
   `(jdee-font-lock-doc-tag-face ((,class (:foreground ,nordisch7))))
   `(jdee-font-lock-link-face ((,class (:underline t))))
   `(jdee-font-lock-modifier-face ((,class (:foreground ,nordisch-keyword))))
   `(jdee-font-lock-number-face ((,class (:foreground ,nordisch-numeric))))
   `(jdee-font-lock-operator-fac ((,class (:foreground ,nordisch-operator))))
   `(jdee-font-lock-package-face ((,class (:foreground ,nordisch-class))))
   `(jdee-font-lock-pre-face ((,class (:foreground ,nordisch-comment :slant italic))))
   `(jdee-font-lock-private-face ((,class (:foreground ,nordisch-keyword))))
   `(jdee-font-lock-public-face ((,class (:foreground ,nordisch-keyword))))
   `(jdee-font-lock-variable-face ((,class (:foreground ,nordisch-variable))))

   ;; JavaScript 2
   `(js2-function-call ((,class (:foreground ,nordisch8))))
   `(js2-private-function-call ((,class (:foreground ,nordisch8))))
   `(js2-jsdoc-html-tag-delimiter ((,class (:foreground ,nordisch6))))
   `(js2-jsdoc-html-tag-name ((,class (:foreground ,nordisch9))))
   `(js2-external-variable ((,class (:foreground ,nordisch4))))
   `(js2-function-param ((,class (:foreground ,nordisch4))))
   `(js2-jsdoc-value ((,class (:foreground ,nordisch-comment))))
   `(js2-jsdoc-tag ((,class (:foreground ,nordisch7))))
   `(js2-jsdoc-type ((,class (:foreground ,nordisch7))))
   `(js2-private-member ((,class (:foreground ,nordisch4))))
   `(js2-object-property ((,class (:foreground ,nordisch4))))
   `(js2-error ((,class (:foreground ,nordisch11))))
   `(js2-warning ((,class (:foreground ,nordisch13))))
   `(js2-instance-member ((,class (:foreground ,nordisch4))))

   ;; JavaScript 3
   `(js3-error-face ((,class (:foreground ,nordisch11))))
   `(js3-external-variable-face ((,class (:foreground ,nordisch4))))
   `(js3-function-param-face ((,class (:foreground ,nordisch4))))
   `(js3-instance-member-face ((,class (:foreground ,nordisch4))))
   `(js3-jsdoc-html-tag-delimiter-face ((,class (:foreground ,nordisch6))))
   `(js3-jsdoc-html-tag-name-face ((,class (:foreground ,nordisch9))))
   `(js3-jsdoc-tag-face ((,class (:foreground ,nordisch9))))
   `(js3-jsdoc-type-face ((,class (:foreground ,nordisch7))))
   `(js3-jsdoc-value-face ((,class (:foreground ,nordisch4))))
   `(js3-magic-paren-face ((,class (:inherit show-paren-match-face))))
   `(js3-private-function-call-face ((,class (:foreground ,nordisch8))))
   `(js3-private-member-face ((,class (:foreground ,nordisch4))))
   `(js3-warning-face ((,class (:foreground ,nordisch13))))

   ;; Rainbow Delimeters
   `(rainbow-delimiters-depth-1-face ((,class :foreground ,nordisch7)))
   `(rainbow-delimiters-depth-2-face ((,class :foreground ,nordisch8)))
   `(rainbow-delimiters-depth-3-face ((,class :foreground ,nordisch9)))
   `(rainbow-delimiters-depth-4-face ((,class :foreground ,nordisch10)))
   `(rainbow-delimiters-depth-5-face ((,class :foreground ,nordisch12)))
   `(rainbow-delimiters-depth-6-face ((,class :foreground ,nordisch13)))
   `(rainbow-delimiters-depth-7-face ((,class :foreground ,nordisch14)))
   `(rainbow-delimiters-depth-8-face ((,class :foreground ,nordisch15)))
   `(rainbow-delimiters-unmatched-face ((,class :foreground ,nordisch4 :background ,nordisch11)))

   ;; Web Mode
   `(web-mode-attr-tag-custom-face ((,class (:foreground ,nordisch-attribute))))
   `(web-mode-builtin-face ((,class (:foreground ,nordisch-keyword))))
   `(web-mode-comment-face ((,class (:foreground ,nordisch-comment :slant italic))))
   `(web-mode-comment-keyword-face ((,class (:foreground ,nordisch-comment :slant italic))))
   `(web-mode-constant-face ((,class (:foreground ,nordisch-variable))))
   `(web-mode-css-at-rule-face ((,class (:foreground ,nordisch-annotation))))
   `(web-mode-css-function-face ((,class (:foreground ,nordisch-method))))
   `(web-mode-css-property-name-face ((,class (:foreground ,nordisch-keyword))))
   `(web-mode-css-pseudo-class-face ((,class (:foreground ,nordisch-class))))
   `(web-mode-css-selector-face ((,class (:foreground ,nordisch-keyword))))
   `(web-mode-css-string-face ((,class (:foreground ,nordisch-string))))
   `(web-mode-doctype-face ((,class (:foreground ,nordisch-preprocessor))))
   `(web-mode-function-call-face ((,class (:foreground ,nordisch-method))))
   `(web-mode-function-name-face ((,class (:foreground ,nordisch-method))))
   `(web-mode-html-attr-name-face ((,class (:foreground ,nordisch-attribute))))
   `(web-mode-html-attr-equal-face ((,class (:foreground ,nordisch-punctuation))))
   `(web-mode-html-attr-value-face ((,class (:foreground ,nordisch-string))))
   `(web-mode-html-entity-face ((,class (:foreground ,nordisch-keyword))))
   `(web-mode-html-tag-bracket-face ((,class (:foreground ,nordisch-punctuation))))
   `(web-mode-html-tag-custom-face ((,class (:foreground ,nordisch-tag))))
   `(web-mode-html-tag-face ((,class (:foreground ,nordisch-tag))))
   `(web-mode-html-tag-namespaced-face ((,class (:foreground ,nordisch-keyword))))
   `(web-mode-json-key-face ((,class (:foreground ,nordisch-class))))
   `(web-mode-json-string-face ((,class (:foreground ,nordisch-string))))
   `(web-mode-keyword-face ((,class (:foreground ,nordisch-keyword))))
   `(web-mode-preprocessor-face ((,class (:foreground ,nordisch-preprocessor))))
   `(web-mode-string-face ((,class (:foreground ,nordisch-string))))
   `(web-mode-symbol-face ((,class (:foreground ,nordisch-variable))))
   `(web-mode-type-face ((,class (:foreground ,nordisch-class))))
   `(web-mode-warning-face ((,class (:inherit ,font-lock-warning-face))))
   `(web-mode-variable-name-face ((,class (:foreground ,nordisch-variable))))

   ;; Anzu
   `(anzu-mode-line ((,class (:foreground, nordisch8))))
   `(anzu-mode-line-no-match ((,class (:foreground, nordisch11))))

   ;; Avy
   `(avy-lead-face ((,class (:background ,nordisch11 :foreground ,nordisch5))))
   `(avy-lead-face-0 ((,class (:background ,nordisch10 :foreground ,nordisch5))))
   `(avy-lead-face-1 ((,class (:background ,nordisch3 :foreground ,nordisch5))))
   `(avy-lead-face-2 ((,class (:background ,nordisch15 :foreground ,nordisch5))))

   ;; Company
   `(company-echo-common ((,class (:foreground ,nordisch0 :background ,nordisch4))))
   `(company-preview ((,class (:foreground ,nordisch4 :background ,nordisch10))))
   `(company-preview-common ((,class (:foreground ,nordisch0 :background ,nordisch8))))
   `(company-preview-search ((,class (:foreground ,nordisch0 :background ,nordisch8))))
   `(company-scrollbar-bg ((,class (:foreground ,nordisch1 :background ,nordisch1))))
   `(company-scrollbar-fg ((,class (:foreground ,nordisch2 :background ,nordisch2))))
   `(company-template-field ((,class (:foreground ,nordisch0 :background ,nordisch7))))
   `(company-tooltip ((,class (:foreground ,nordisch4 :background ,nordisch2))))
   `(company-tooltip-annotation ((,class (:foreground ,nordisch12))))
   `(company-tooltip-annotation-selection ((,class (:foreground ,nordisch12 :weight bold))))
   `(company-tooltip-common ((,class (:foreground ,nordisch8))))
   `(company-tooltip-common-selection ((,class (:foreground ,nordisch8 :background ,nordisch3))))
   `(company-tooltip-mouse ((,class (:inherit highlight))))
   `(company-tooltip-selection ((,class (:background ,nordisch3 :weight bold))))

   ;; diff-hl
   `(diff-hl-change ((,class (:background ,nordisch-gg-changed))))
   `(diff-hl-insert ((,class (:background ,nordisch-gg-new))))
   `(diff-hl-delete ((,class (:background ,nordisch-gg-deleted))))

   ;; Ediff
   `(ediff-even-diff-A ((,class (:background ,nordisch3)))) ;; overview highlights
   `(ediff-even-diff-B ((,class (:background ,nordisch3))))
   `(ediff-even-diff-C ((,class (:background ,nordisch3))))
   `(ediff-odd-diff-A  ((,class (:background ,nordisch1))))
   `(ediff-odd-diff-B  ((,class (:background ,nordisch1))))
   `(ediff-odd-diff-C  ((,class (:background ,nordisch1))))
   `(ediff-fine-diff-A ((,class (:background ,nordisch11 :foreground ,nordisch0)))) ;; current diff face
   `(ediff-fine-diff-B ((,class (:background ,nordisch14 :foreground ,nordisch0))))
   `(ediff-fine-diff-C ((,class (:background ,nordisch13 :foreground ,nordisch0))))
   ;; `(ediff-current-diff-A ((,class (:background ,nordisch12 :foreground ,nordisch0))))  ;; current beckground
   ;; `(ediff-current-diff-B ((,class (:background ,nordisch13 :foreground ,nordisch0))))
   ;; `(ediff-current-diff-C ((,class (:background ,nordisch14 :foreground ,nordisch0))))

   ;; Dired
   `(dired-subtree-depth-1-face ((,class (:background ,nordisch-dark-1 :extend t))))
   `(dired-subtree-depth-2-face ((,class (:background ,nordisch-dark-2 :extend t))))
   `(dired-subtree-depth-3-face ((,class (:background ,nordisch-dark-3 :extend t))))
   `(dired-subtree-depth-4-face ((,class (:background ,nordisch-dark-4 :extend t))))
   `(dired-subtree-depth-5-face ((,class (:background ,nordisch-dark-5 :extend t))))
   `(dired-subtree-depth-6-face ((,class (:background ,nordisch-dark-5 :extend t))))
   `(dired-subtree-depth-7-face ((,class (:background ,nordisch-dark-5 :extend t))))
   `(dired-subtree-depth-8-face ((,class (:background ,nordisch-dark-5 :extend t))))
   `(dired-subtree-depth-9-face ((,class (:background ,nordisch-dark-5 :extend t))))

   ;; diredfl
   `(diredfl-dir-heading ((,class (:background ,nordisch-light-4 :foreground ,nordisch6 :extend t))))
   `(diredfl-dir-name ((,class (:foreground ,nordisch8))))
   `(diredfl-file-name ((,class (:foreground ,nordisch5))))
   `(diredfl-file-suffix ((,class (:foreground ,nordisch-icy))))
   ;; `(diredfl-deletion ((,class (:foreground ,nordisch13 :background ,nordisch-darkred))))
   `(diredfl-deletion ((,class (:foreground ,nordisch4 :background ,nordisch0))))
   `(diredfl-deletion-file-name ((,class (:foreground ,nordisch12))))
   `(diredfl-symlink ((,class (:foreground ,nordisch7))))
   `(diredfl-date-time ((,class (:foreground ,nordisch7))))
   `(diredfl-number ((,class (:foreground ,nordisch-variable))))
   `(diredfl-no-priv ((,class (:foreground ,nordisch4))))
   `(diredfl-read-priv ((,class (:foreground ,nordisch4))))
   `(diredfl-write-priv ((,class (:foreground ,nordisch8))))
   `(diredfl-exec-priv ((,class (:foreground ,nordisch-icy))))
   `(diredfl-dir-priv ((,class (:foreground ,nordisch13))))
   `(diredfl-link-priv ((,class (:foreground ,nordisch13))))
   `(diredfl-rare-priv ((,class (:foreground ,nordisch13))))
   `(diredfl-other-priv ((,class (:foreground ,nordisch13))))
   `(diredfl-ignored-file-name ((,class (:foreground ,nordisch4))))
   `(diredfl-compressed-file-suffix ((,class (:foreground ,nordisch-constant))))

   ;; eshell
   `(eshell-prompt ((,class (:foreground ,nordisch8))))
   `(eshell-ls-archive ((,class (:foreground ,nordisch13))))
   `(eshell-ls-backup ((,class (:foreground ,nordisch-constant))))
   ;; `(eshell-ls-clutter ((,class (:foreground ,nordisch7))))
   `(eshell-ls-directory ((,class (:foreground ,nordisch8 :weight bold))))
   `(eshell-ls-executable ((,class (:foreground ,nordisch14))))
   ;; `(eshell-ls-missing ((,class (:foreground ,nordisch7))))
   ;; `(eshell-ls-product ((,class (:foreground ,nordisch7))))
   `(eshell-ls-readonly ((,class (:foreground ,nordisch-variable))))
   `(eshell-ls-special ((,class (:foreground ,nordisch15))))
   `(eshell-ls-symlink ((,class (:foreground ,nordisch9))))
   `(eshell-ls-unreadable ((,class (:foreground ,nordisch11))))

   ;; Elfeed
   `(elfeed-search-date-face ((,class (:foreground ,nordisch9))))
   `(elfeed-search-unread-title-face ((,class (:foreground ,nordisch6 :weight bold))))
   `(elfeed-search-title-face ((,class (:foreground ,nordisch4))))
   `(elfeed-search-feed-face ((,class (:foreground ,nordisch7))))
   `(elfeed-search-tag-face ((,class (:foreground ,nordisch-variable))))

   ;; Evil
   `(evil-ex-info ((,class (:foreground ,nordisch8))))
   `(evil-ex-substitute-replacement ((,class (:foreground ,nordisch9))))
   `(evil-ex-substitute-matches ((,class (:inherit isearch))))

   ;; Flycheck
   `(flycheck-error ((,class (:underline (:style wave :color ,nordisch11)))))
   `(flycheck-fringe-error ((,class (:foreground ,nordisch11 :weight bold))))
   `(flycheck-fringe-info ((,class (:foreground ,nordisch8 :weight bold))))
   `(flycheck-fringe-warning ((,class (:foreground ,nordisch13 :weight bold))))
   `(flycheck-info ((,class (:underline (:style wave :color ,nordisch8)))))
   `(flycheck-warning ((,class (:underline (:style wave :color ,nordisch13)))))

   ;; Git Gutter
   `(git-gutter:deleted ((,class (:foreground ,nordisch-gg-deleted))))
   `(git-gutter:modified ((,class (:foreground ,nordisch-gg-changed))))
   `(git-gutter:added ((,class (:foreground ,nordisch-gg-new))))

   ;; Git Gutter Plus
   `(git-gutter+-modified ((,class (:foreground ,nordisch13))))
   `(git-gutter+-added ((,class (:foreground ,nordisch14))))
   `(git-gutter+-deleted ((,class (:foreground ,nordisch11))))

   ;; Helm
   `(helm-bookmark-addressbook ((,class (:foreground ,nordisch7))))
   `(helm-bookmark-directory ((,class (:foreground ,nordisch9))))
   `(helm-bookmark-file ((,class (:foreground ,nordisch8))))
   `(helm-bookmark-gnus ((,class (:foreground ,nordisch10))))
   `(helm-bookmark-info ((,class (:foreground ,nordisch14))))
   `(helm-bookmark-man ((,class (:foreground ,nordisch4))))
   `(helm-bookmark-w3m ((,class (:foreground ,nordisch9))))
   `(helm-buffer-directory ((,class (:foreground ,nordisch9))))
   `(helm-buffer-file ((,class (:foreground ,nordisch8))))
   `(helm-buffer-not-saved ((,class (:foreground ,nordisch13))))
   `(helm-buffer-process ((,class (:foreground ,nordisch10))))
   `(helm-candidate-number ((,class (:foreground ,nordisch4 :weight bold))))
   `(helm-candidate-number-suspended ((,class (:foreground ,nordisch4))))
   `(helm-ff-directory ((,class (:foreground ,nordisch9 :weight bold))))
   `(helm-ff-dirs ((,class (:foreground ,nordisch9))))
   `(helm-ff-dotted-director ((,class (:foreground ,nordisch9 :underline t))))
   `(helm-ff-dotted-symlink-director ((,class (:foreground ,nordisch7 :weight bold))))
   `(helm-ff-executable ((,class (:foreground ,nordisch8))))
   `(helm-ff-file ((,class (:foreground ,nordisch4))))
   `(helm-ff-invalid-symlink ((,class (:foreground ,nordisch11 :weight bold))))
   `(helm-ff-prefix ((,class (:foreground ,nordisch0 :background ,nordisch9))))
   `(helm-ff-symlink ((,class (:foreground ,nordisch7))))
   `(helm-grep-cmd-line ((,class (:foreground ,nordisch4 :background ,nordisch0))))
   `(helm-grep-file ((,class (:foreground ,nordisch8))))
   `(helm-grep-finish ((,class (:foreground ,nordisch5))))
   `(helm-grep-lineno ((,class (:foreground ,nordisch4))))
   `(helm-grep-match ((,class (:inherit isearch))))
   `(helm-grep-running ((,class (:foreground ,nordisch8))))
   `(helm-header ((,class (:foreground ,nordisch9 :background ,nordisch2))))
   `(helm-header-line-left-margin ((,class (:foreground ,nordisch9 :background ,nordisch2))))
   `(helm-history-deleted ((,class (:foreground ,nordisch11))))
   `(helm-history-remote ((,class (:foreground ,nordisch4))))
   `(helm-lisp-completion-info ((,class (:foreground ,nordisch4 :weight bold))))
   `(helm-lisp-show-completion ((,class (:inherit isearch))))
   `(helm-locate-finish ((,class (:foreground ,nordisch14))))
   `(helm-match ((,class (:foreground ,nordisch8))))
   `(helm-match-item ((,class (:inherit isearch))))
   `(helm-moccur-buffer ((,class (:foreground ,nordisch8))))
   `(helm-resume-need-update ((,class (:foreground ,nordisch0 :background ,nordisch13))))
   `(helm-selection ((,class (:inherit highlight))))
   `(helm-selection-line ((,class (:background ,nordisch2))))
   `(helm-source-header ((,class (:height 1.44 :foreground ,nordisch8 :background ,nordisch2))))
   `(helm-swoop-line-number-face ((,class (:foreground ,nordisch4 :background ,nordisch0))))
   `(helm-swoop-target-word-face ((,class (:foreground ,nordisch0 :background ,nordisch7))))
   `(helm-swoop-target-line-face ((,class (:background ,nordisch13 :foreground ,nordisch3))))
   `(helm-swoop-target-line-block-face ((,class (:background ,nordisch13 :foreground ,nordisch3))))
   `(helm-separator ((,class (:background ,nordisch2))))
   `(helm-visible-mark ((,class (:background ,nordisch2))))

   ;; Magit
   `(magit-branch ((,class (:foreground ,nordisch7 :weight bold))))
   `(magit-diff-context-highlight ((,class (:background ,nordisch-light-1))))
   `(magit-diff-file-header ((,class (:foreground ,nordisch8 :box (:color ,nordisch8)))))
   `(magit-diffstat-added ((,class (:foreground ,nordisch14))))
   `(magit-diffstat-removed ((,class (:foreground ,nordisch11))))
   `(magit-diff-added ((,class (:foreground ,nordisch14))))
   `(magit-diff-removed ((,class (:foreground ,nordisch12))))
   `(magit-diff-added-highlight ((,class (:foreground ,nordisch14 :background ,nordisch-light-1))))
   `(magit-diff-removed-highlight ((,class (:foreground ,nordisch12 :background ,nordisch-light-1))))
   `(magit-diff-hunk-heading ((,class (:background ,nordisch-light-3))))
   `(magit-diff-hunk-heading-highlight ((,class (:background ,nordisch-light-6))))
   `(magit-hash ((,class (:foreground ,nordisch8))))
   `(magit-hunk-heading ((,class (:foreground ,nordisch9))))
   `(magit-hunk-heading-highlight ((,class (:foreground ,nordisch9 :background ,nordisch-light-3))))
   `(magit-item-highlight ((,class (:foreground ,nordisch8 :background ,nordisch-light-3))))
   `(magit-log-author ((,class (:foreground ,nordisch7))))
   `(magit-process-ng ((,class (:foreground ,nordisch13 :weight bold))))
   `(magit-process-ok ((,class (:foreground ,nordisch14 :weight bold))))
   `(magit-section-heading ((,class (:foreground ,nordisch7 :weight bold))))
   `(magit-section-highlight ((,class (:background ,nordisch-light-3))))

   ;; MU4E
   `(mu4e-header-marks-face ((,class (:foreground ,nordisch9))))
   `(mu4e-title-face ((,class (:foreground ,nordisch8))))
   `(mu4e-header-key-face ((,class (:foreground ,nordisch8))))
   `(mu4e-highlight-face ((,class (:highlight))))
   `(mu4e-flagged-face ((,class (:foreground ,nordisch13))))
   `(mu4e-unread-face ((,class (:foreground ,nordisch13 :weight bold))))
   `(mu4e-link-face ((,class (:underline t))))

   ;; Powerline
   `(powerline-active1 ((,class (:foreground ,nordisch4 :background ,nordisch1))))
   `(powerline-active2 ((,class (:foreground ,nordisch4 :background ,nordisch3))))
   `(powerline-inactive1 ((,class (:background ,nordisch2))))
   `(powerline-inactive2 ((,class (:background ,nordisch2))))

   ;; Powerline Evil
   `(powerline-evil-base-face ((,class (:foreground ,nordisch4))))
   `(powerline-evil-normal-face ((,class (:background ,nordisch8))))
   `(powerline-evil-insert-face ((,class (:foreground ,nordisch0 :background ,nordisch4))))
   `(powerline-evil-visual-face ((,class (:foreground ,nordisch0 :background ,nordisch7))))
   `(powerline-evil-replace-face ((,class (:foreground ,nordisch0 :background ,nordisch9))))

   ;; NeoTree
   `(neo-banner-face ((,class (:foreground ,nordisch10))))
   `(neo-dir-link-face ((,class (:foreground ,nordisch9))))
   `(neo-expand-btn-face ((,class (:foreground ,nordisch6 :bold t))))
   `(neo-file-link-face ((,class (:foreground ,nordisch4))))
   `(neo-root-dir-face ((,class (:foreground ,nordisch7 :weight bold))))
   `(neo-vc-added-face ((,class (:foreground ,nordisch14))))
   `(neo-vc-conflict-face ((,class (:foreground ,nordisch11))))
   `(neo-vc-default-face ((,class (:foreground ,nordisch4))))
   `(neo-vc-edited-face ((,class (:foreground ,nordisch13))))
   `(neo-vc-ignored-face ((,class (:foreground ,nordisch3))))
   `(neo-vc-missing-face ((,class (:foreground ,nordisch12))))
   `(neo-vc-needs-merge-face ((,class (:background ,nordisch12 :foreground ,nordisch4))))
   `(neo-vc-needs-update-face ((,class (:background ,nordisch10 :foreground ,nordisch4))))
   `(neo-vc-removed-face ((,class (:foreground ,nordisch11 :strike-through nil))))
   `(neo-vc-up-to-date-face ((,class (:foreground ,nordisch4))))
   `(neo-vc-user-face ((,class (:foreground ,nordisch4))))

   ;; Treemacs
   `(treemacs-fringe-indicator-face ((,class (:foreground ,nordisch-cursor))))
   `(treemacs-hl-line-face ((,class (:background ,nordisch-hl-bg :weight bold))))

   ;; Cider
   `(cider-result-overlay-face ((t (:background unspecified))))

   ;; Sly
   `(sly-mrepl-output-face ((,class (:foreground ,nordisch4))))

   ;; rg
   ;; `(rg-match-face ((,class (:background ,nordisch-light-6 :box (:line-width 1 :color ,nordisch8)))))
   ;; `(rg-match-face ((,class (:background ,nordisch-light-4 :underline ,nordisch9))))
   `(rg-match-face ((,class (:foreground ,nordisch13))))

   ;; Markdown
   `(markdown-blockquote-face ((,class (:foreground ,nordisch-comment))))
   `(markdown-bold-face ((,class (:inherit bold))))
   `(markdown-header-face-1 ((,class (:foreground ,nordisch7 :weight bold :height 1.3))))
   `(markdown-header-face-2 ((,class (:foreground ,nordisch8 :weight bold :height 1.3))))
   `(markdown-header-face-3 ((,class (:foreground ,nordisch9 :weight bold :height 1.2))))
   `(markdown-header-face-4 ((,class (:foreground ,nordisch10 :weight bold :height 1.1))))
   `(markdown-header-face-5 ((,class (:inherit markdown-header-face-4 :height 1.1))))
   `(markdown-header-face-6 ((,class (:inherit markdown-header-face-5))))
   `(markdown-inline-code-face ((,class (:foreground ,nordisch7))))
   `(markdown-italic-face ((,class (:inherit italic))))
   `(markdown-link-face ((,class (:foreground ,nordisch8))))
   `(markdown-markup-face ((,class (:foreground ,nordisch9))))
   `(markdown-reference-face ((,class (:inherit markdown-link-face))))
   `(markdown-url-face ((,class (:foreground ,nordisch4 :underline t))))
   ;; `(markdown-code-face ((,class (:foreground ,nordisch4 :background ,nordisch-dark-1 :extend t))))
   `(markdown-code-face ((,class (:foreground ,nordisch4))))
   ;; `(markdown-pre-face ((,class (:foreground ,nordisch13-dark :background ,nordisch-dark-1.5 :extend t))))
   `(markdown-pre-face ((,class (:foreground ,nordisch13))))
   `(markdown-table-face ((,class (:foreground ,nordisch4 :background ,nordisch-dark-1))))

   ;; Org
   `(org-level-1 ((,class (:foreground ,nordisch7 :weight bold :height 1.3))))
   `(org-level-2 ((,class (:foreground ,nordisch8 :weight bold :height 1.2))))
   `(org-level-3 ((,class (:foreground ,nordisch9 :weight bold :height 1.1))))
   `(org-level-4 ((,class (:foreground ,nordisch10 :weight bold :height 1.1))))
   `(org-level-5 ((,class (:inherit org-level-4 :height 1.05))))
   `(org-level-6 ((,class (:inherit org-level-4))))
   `(org-level-7 ((,class (:inherit org-level-4))))
   `(org-level-8 ((,class (:inherit org-level-4))))
   `(org-document-title ((,class (:foreground ,nordisch8 :weight bold :height 1.2))))
   `(org-document-info ((,class (:foreground ,nordisch4))))
   `(org-document-info-keyword ((,class (:foreground ,nordisch7 :weight bold :slant italic))))
   `(org-meta-line ((,class (:foreground ,nordisch-comment :slant italic))))
   `(org-headline-done ((,class (:foreground ,nordisch-string))))
   `(org-agenda-date ((,class (:foreground ,nordisch8 :underline nil))))
   `(org-agenda-date-weekend ((,class (:foreground ,nordisch9))))
   `(org-agenda-date-today ((,class (:foreground ,nordisch8 :weight bold))))
   `(org-agenda-dimmed-todo-face ((,class (:background ,nordisch13))))
   `(org-agenda-done ((,class (:foreground ,nordisch14))))
   `(org-agenda-structure ((,class (:foreground ,nordisch9))))
   `(org-block-begin-line ((,class (:background ,nordisch-dark-1 :foreground ,nordisch7 :slant italic :extend t))))
   `(org-block ((,class (:background ,nordisch-dark-1.5 :foreground ,nordisch4 :extend t))))
   `(org-block-end-line ((,class (:background ,nordisch-dark-1 :foreground ,nordisch7 :slant italic :extend t))))
   `(org-checkbox ((,class (:foreground ,nordisch9))))
   `(org-checkbox-statistics-done ((,class (:foreground ,nordisch14))))
   `(org-checkbox-statistics-todo ((,class (:foreground ,nordisch13))))
   `(org-code ((,class (:foreground ,nordisch14))))
   `(org-column ((,class (:background ,nordisch2))))
   `(org-column-title ((,class (:inherit org-column :weight bold :underline t))))
   `(org-date ((,class (:foreground ,nordisch8))))
   `(org-done ((,class (:foreground ,nordisch14 :weight bold))))
   `(org-ellipsis ((,class (:foreground ,nordisch3))))
   `(org-footnote ((,class (:foreground ,nordisch8))))
   `(org-formula ((,class (:foreground ,nordisch9))))
   `(org-hide ((,class (:foreground ,nordisch0 :background ,nordisch0))))
   `(org-indent ((,class (:foreground ,nordisch3 :background ,nordisch0))))
   `(org-link ((,class (:underline t :foreground ,nordisch4 :slant italic))))
   `(org-macro ((,class (:foreground ,nordisch13))))
   `(org-quote ((,class (:inherit org-block :slant italic))))
   `(org-scheduled ((,class (:foreground ,nordisch14))))
   `(org-scheduled-previously ((,class (:foreground ,nordisch13))))
   `(org-scheduled-today ((,class (:foreground ,nordisch8))))
   `(org-special-keyword ((,class (:foreground ,nordisch9))))
   `(org-sexp-date ((,class (:foreground ,nordisch7))))
   `(org-table ((,class (:foreground ,nordisch4 :background ,nordisch-dark-1))))
   `(org-todo ((,class (:foreground ,nordisch13 :weight bold))))
   `(org-upcoming-deadline ((,class (:foreground ,nordisch12))))
   `(org-verse ((,class (:inherit org-block :slant italic))))
   `(org-verbatim ((,class (:foreground ,nordisch7))))
   `(org-warning ((,class (:foreground ,nordisch13 :weight bold))))

   ;; IDO
   `(ido-only-match ((,class (:foreground ,nordisch8))))
   `(ido-first-match ((,class (:foreground ,nordisch8 :weight bold))))
   `(ido-subdir ((,class (:foreground ,nordisch9))))

   ;; ivy-mode
   `(ivy-current-match ((,class (:inherit region))))
   `(ivy-minibuffer-match-face-1 ((,class (:inherit default))))
   `(ivy-minibuffer-match-face-2 ((,class (:background ,nordisch7 :foreground ,nordisch0))))
   `(ivy-minibuffer-match-face-3 ((,class (:background ,nordisch8 :foreground ,nordisch0))))
   `(ivy-minibuffer-match-face-4 ((,class (:background ,nordisch9 :foreground ,nordisch0))))
   ;; `(ivy-minibuffer-match-face-2 ((,class (:background ,nordisch3 :foreground ,nordisch15-bright :underline t))))
   ;; `(ivy-minibuffer-match-face-3 ((,class (:background ,nordisch3 :foreground ,nordisch14-bright :underline t))))
   ;; `(ivy-minibuffer-match-face-4 ((,class (:background ,nordisch3 :foreground ,nordisch13-bright :underline t))))
   `(ivy-remote ((,class (:foreground ,nordisch14))))
   `(ivy-posframe ((,class (:background ,nordisch1))))
   `(ivy-posframe-border ((,class (:background ,nordisch1))))
   `(ivy-remote ((,class (:foreground ,nordisch14))))

   ;; perspective
   `(persp-selected-face ((,class (:foreground ,nordisch8 :weight bold))))

   ;; StackExchange
   `(sx-question-mode-content-face ((,class (:background ,nordisch-dark-1 :extend t))))

   ;; log4j
   `(log4j-font-lock-info-face ((,class (:foreground ,nordisch14))))
   `(log4j-font-lock-debug-face ((,class (:foreground ,nordisch9))))
   `(log4j-font-lock-warn-face ((,class (:foreground ,nordisch13))))
   `(log4j-font-lock-error-face ((,class (:foreground ,nordisch12))))
   `(log4j-font-lock-fatal-face ((,class (:foreground ,nordisch11))))
   `(log4j-font-lock-keywords-face ((,class (:foreground ,nordisch-keyword))))
   `(log4j-font-lock-config-face ((,class (:foreground ,nordisch-constant))))))

;;;###autoload
(when (and (boundp 'custom-theme-load-path) load-file-name)
  (add-to-list 'custom-theme-load-path
               (file-name-as-directory (file-name-directory load-file-name))))

(provide-theme 'nordisch)

;; Local Variables:
;; no-byte-compile: t
;; indent-tabs-mode: nil
;; End:

;;; nordisch-theme.el ends here
