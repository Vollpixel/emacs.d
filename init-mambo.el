;;;;
;;          init-mambo.el
;; Emacs configuration for test systems
;;;;


;; HTTPS proxy settings that differ between systems.
;; (let ((my-proxy-settings-file "~/.emacs.d/proxy-settings.el"))
;;   (when (file-exists-p my-proxy-settings-file)
;;     (load-file my-proxy-settings-file)))

;; Load local settings that may differ between systems
;; (let ((my-local-settings-file "~/.emacs.d/local-settings.el"))
;;   (when (file-exists-p my-local-settings-file)
;;     (load-file my-local-settings-file)))


;; Packages
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Define package repositories
(require 'package)

;; (add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/") t)
;; (add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/") t)

(setq package-archives
      '(("gnu" . "https://elpa.gnu.org/packages/")
        ("melpa" . "https://melpa.org/packages/")
        ("melpa-stable" . "https://stable.melpa.org/packages/")
        ("nongnu" . "https://elpa.nongnu.org/nongnu/")))

;; Load and activate emacs packages
(package-initialize)

;; Download the ELPA archive description if needed
(when (not package-archive-contents)
  (package-refresh-contents))

;; Install wanted packages. These can also be installed with M-x package-install
(defvar my-packages
  '(
    ;; Modes
    dockerfile-mode
    json-mode
    yaml-mode

    ;; Modules
    lispy
    highlight-symbol
    multiple-cursors
    swiper
    counsel
    which-key
    neotree

    ;; Dired
    dired-collapse
    dired-subtree

    ;; Tools
    ;;kubel
    ;;docker
    ;;docker-tramp
    keystore-mode
    shell-pop
    ;;logview
    log4j-mode
    jdecomp
    restclient
    x509-mode

    ;; UI
    smart-mode-line
    flatland-theme
    ))

;; Install packages, that are not already installed
(dolist (p my-packages)
  (when (not (package-installed-p p))
    (package-install p)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Customization files
;;;;;;;;;;;;;;;;;;;;;;;;

;; Some custom settings and directories:

(add-to-list 'load-path "~/.emacs.d/vendor")
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")
(add-to-list 'load-path "~/.emacs.d/themes")


;; Theme
;;;;;;;;;;

(load-theme 'nordisch t)


;; Tools
;;;;;;;;;;

(require 'keystore-mode)


;; UI
;;;;;;;

;; Modeline
(require 'smart-mode-line)
;;(setq sml/theme 'respectful)
(setq sml/theme 'dark)
(setq sml/no-confirm-load-theme t)
(sml/setup)

;; Cursor
(blink-cursor-mode -1)
(set-cursor-color "#ffe7a6")

(when (display-graphic-p)
  (global-hl-line-mode t))

;; Keep syntax highlighting
(set-face-foreground 'highlight nil)

;;(global-linum-mode)
;;(setq linum-format "%4d\u2502")

(when (fboundp 'tool-bar-mode)
  (tool-bar-mode -1))

(menu-bar-mode -1)


;; Custom variables
;;;;;;;;;;;;;;;;;;;;;

;; Custom variables are also defined in ~/.emacs.d/settings.el
(setq custom-file "~/.emacs.d/settings.el")

(if (file-exists-p custom-file)
    (load custom-file))

;; Changes all yes/no questions to y/n type
(fset 'yes-or-no-p 'y-or-n-p)

;; Use UTF-8 as default encoding
(prefer-coding-system 'utf-8)
(setq-default buffer-file-coding-system 'utf-8-unix)

;; Jump into help window
(setq help-window-select t)

(setq make-backup-files nil)
(setq auto-save-default nil)

;; Warn only for really large files
(setq large-file-warning-threshold 150000000)

;; System PATH
(when (string-equal system-type "windows-nt")
  (setenv "PATH" (concat (getenv "PATH") ";"
                         "C:\\Program Files\\Git\\usr\\bin" ";"
                         "C:\\Users\\pestoldt.CORPDOM\\bin")))

(setq create-lockfiles nil)
;; Go straight to scratch buffer on startup
(setq inhibit-startup-message t)

;; NO BELL !!!
(setq ring-bell-function 'ignore)

;; Start scrolling slightly before end of window
(setq scroll-margin 4
      scroll-step 1
      scroll-conservatively 10000)

;; Newline at end of file
(setq require-final-newline t)

;; Enable commands to change case (C-x C-u, C-x C-l)
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)
;; Enable narrow region
(put 'narrow-to-region 'disabled nil)

;; OS interaction
(setq  save-interprogram-paste-before-kill t
       apropos-do-all t
       mouse-yank-at-point t)

;; Enable which-key
(which-key-mode)

;; In dired show directories first
(if (eq system-type 'gnu/linux)
    (setq dired-listing-switches "-alh --group-directories-first"))

;; Docker & Kubernetes
;; (setq kubel-namespace (getenv "NS"))

(add-hook 'after-save-hook
          'executable-make-buffer-file-executable-if-script-p)

(let ((my-database-settings-file "~/.emacs.d/database-settings.el"))
  (when (file-exists-p my-database-settings-file)
    (load-file my-database-settings-file)))


;; Indentation
;;;;;;;;;;;;;;;;

(setq tab-width 4)

;; Use Tab to Indent or Complete
(setq tab-always-indent 'complete)

;; Don't use hard tabs
(setq-default indent-tabs-mode nil)

;; default indentation
(setq-default sh-basic-offset 4)
(setq-default sh-indentation 4)

(add-hook 'java-mode-hook
          (lambda ()
            (setq c-basic-offset 4
                  tab-width 4)))


;; Dired
;;;;;;;;;;

(if (eq system-type 'gnu/linux)
    (setq dired-listing-switches "-alh --group-directories-first"))

(require 'dired-collapse)
(require 'dired-subtree)

(add-hook 'dired-mode-hook (lambda () (local-set-key (kbd "<SPC>") 'dired-subtree-toggle)))
;; TAB does not work
(add-hook 'dired-mode-hook (lambda () (local-set-key (kbd "[?\t]") 'dired-subtree-toggle)))

;; Navigation
;;;;;;;;;;;;;;;

(setq neo-window-fixed-size nil
      neo-window-width 40)

(global-set-key (kbd "C-c n t") 'neotree-toggle)


;; Unique buffer names
;;;;;;;;;;;;;;;;;;;;;;;;

;; Prevent identical buffer names when files have identical names
;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Uniquify.html
(require 'uniquify)
(setq uniquify-buffer-name-style 'forward)
;; from prelude: https://github.com/bbatsov/prelude
(setq uniquify-separator "/")
(setq uniquify-after-kill-buffer-p t)    ; rename after killing uniquified
(setq uniquify-ignore-buffers-re "^\\*") ; don't muck with special buffers


;; Lisp settings
;;;;;;;;;;;;;;;;;;

(add-hook 'emacs-lisp-mode-hook       #'lispy-mode)
(add-hook 'eval-expression-minibuffer-setup-hook #'lispy-mode)
(add-hook 'lisp-mode-hook             #'lispy-mode)
(add-hook 'lisp-interaction-mode-hook #'lispy-mode)
(add-hook 'scheme-mode-hook           #'lispy-mode)


;; Ivy, Counsel, Swiper
;;;;;;;;;;;;;;;;;;;;;;;;;

;; Disable ido completely
(ido-mode -1)

;; Enable ivy
(ivy-mode 1)

;; add ‘recentf-mode’ and bookmarks to ‘ivy-switch-buffer’.
(setq ivy-use-virtual-buffers t)
(setq ivy-height 24)  ;; number of result lines to display
(setq enable-recursive-minibuffers t)
(setq ivy-use-selectable-prompt t)
(setq ivy-count-format "[%d/%d] ")
(setq ivy-display-style 'fancy)

;; Change behavior of ivy occur
;; See: https://oremacs.com/2017/11/18/dired-occur/
(ivy-set-occur 'ivy-switch-buffer 'ivy-switch-buffer-occur)

;; Support functions
(defun ivy-switch-buffer-occur ()
  "Occur function for `ivy-switch-buffer' using `ibuffer'."
  (ibuffer nil (buffer-name)
           (list (cons 'name ivy--old-re))))

;; Swiper
;; Advise swiper to recenter on exit
(defun my/swiper-recenter (&rest args)
  "recenter display after swiper"
  (recenter))

(advice-add 'swiper :after #'my/swiper-recenter)


;; eshell
;;;;;;;;;;;

(setq eshell-prompt-regexp "^[^#$\n]*[#$] "
      eshell-prompt-function
      (lambda ()
        (concat "[" (user-login-name) "@" (system-name) " "
                (if (string= (eshell/pwd) (getenv "HOME"))
                    "~" (eshell/basename (eshell/pwd)))
                "]"
                (if (= (user-uid) 0) "# " "$ "))))

;; shell-popup
(custom-set-variables
 '(shell-pop-term-shell "/bin/bash")
 ;; '(shell-pop-shell-type (quote ("shell" "*shell*" (lambda () (shell)))))
 ;; '(shell-pop-shell-type (quote ("eshell" "*eshell*" (lambda () (eshell)))))
 '(shell-pop-shell-type (quote ("ansi-term" "*ansi-term*" (lambda nil (ansi-term shell-pop-term-shell)))))
 '(shell-pop-autocd-to-working-dir nil)
 '(shell-pop-window-size 40)
 '(shell-pop-full-span t)
 '(shell-pop-window-position "bottom"))

;; Logs
;;;;;;;;;

(require 'log4j-mode)

(add-hook #'log4j-mode-hook #'view-mode)
(add-hook #'log4j-mode-hook #'read-only-mode)

;; (require 'logview)
;;
;; (add-to-list 'logview-std-submodes '("OBA" . ((format  . "TIMESTAMP LEVEL [THREAD] NAME -")
;;                                               (levels  . "SLF4J")
;;                                               (aliases . ("opsa")))))
;;
;; (add-to-list 'logview-std-submodes '("OBM" . ((format  . "TIMESTAMP [THREAD] (NAME) LEVEL  -")
;;                                               (levels  . "SLF4J")
;;                                               (aliases . ("omi")))))
;;
;; (setq logview-highlighted-entry-part 'header)


;; Java decompile
;; --------------

(require 'jdecomp)
(customize-set-variable 'jdecomp-decompiler-type 'cfr)
(customize-set-variable 'jdecomp-decompiler-paths
                        '((cfr . "~/bin/cfr.jar")
                          (fernflower . "/opt/idea-IU/plugins/java-decompiler/lib/java-decompiler.jar")
                          (procyon . "/tmp/procyon-decompiler-0.5.30.jar")))
(add-to-list 'auto-mode-alist '("\\.class$" . jdecomp-mode))
(jdecomp-mode nil)


;; Diff settings
;;;;;;;;;;;;;;;;;;

;; ediff
;; (setq ediff-split-window-function 'split-window-horizontally)
(setq ediff-split-window-function 'split-window-vertically)

;; Use Emacs as diff-tool: https://www.emacswiki.org/emacs/EdiffMode
;; Usage: emacs -ediff file1 file2
(defun command-line-ediff (switch)
  (let ((file1 (pop command-line-args-left))
        (file2 (pop command-line-args-left)))
    (ediff-files file1 file2)))

(add-to-list 'command-switch-alist '("-ediff" . command-line-ediff))


;; Editing
;;;;;;;;;;;;

;; Don't save file status
(setq desktop-save-mode nil)

;; Put all backups in ~/.emacs.d/backups
;; http://www.gnu.org/software/emacs/manual/html_node/elisp/Backup-Files.html
(setq backup-directory-alist `(("." . ,(concat user-emacs-directory "backups"))))
(setq auto-save-default nil)

;; (setq electric-indent-mode nil)
(setq electric-indent-mode t)

;; Automatically replace selection
(delete-selection-mode 1)

;; Don't highlight trailing whitespaces
(custom-set-variables
 '(show-trailing-whitespace nil))

;; Highlight symbols
(require 'highlight-symbol)

(require 'dockerfile-mode)
(add-to-list 'auto-mode-alist '("Dockerfile\\'" . dockerfile-mode))


;; Functions
;;;;;;;;;;;;;;

;; (defun my/bol-or-indent ()
;;   "Move cursor to beginning of line or to indentation"
;;   (interactive)
;;   (if (bolp)
;;       (back-to-indentation)
;;     (beginning-of-line)))

(defun my/point-at-indentation ()
  "Return non-nil if point is at indentation, nil otherwise."
  (= (save-excursion (back-to-indentation) (point)) (point)))

(defun my/bol-or-indent ()
  "Toggle between beginning of line and point of indentation."
  (interactive)
  (if (my/point-at-indentation)
      (beginning-of-line)
    (back-to-indentation)))

;; Duplicate line under cursor (from EmacsWiki)

(defun my/duplicate-current-line (&optional n)
  "Duplicate current line, make more than 1 copy given a numeric argument."
  (interactive "p")
  (save-excursion
    (let ((nb (or n 1))
          (current-line (thing-at-point 'line)))
      ;; when on last line, insert a newline first
      (when (or (= 1 (forward-line 1)) (eq (point) (point-max)))
        (newline))
      ;; now insert as many time as requested
      (while (> n 0)
        (insert current-line)
        (decf n)))))

(defun my/move-line-up ()
  (interactive)
  (transpose-lines 1)
  (previous-line 2))

(defun my/move-line-down ()
  (interactive)
  (next-line 1)
  (transpose-lines 1)
  (previous-line 1))

;; dired

(defvar my-dired-show-hidden t)
(defvar my-dired-hide-hidden-switches "-lh --group-directories-first --time-style=long-iso")

(defun my/dired-toggle-hidden-files-view ()
  (interactive)
  (setq my-dired-show-hidden (not my-dired-show-hidden))
  (if (eq my-dired-show-hidden t)
      (setq dired-actual-switches dired-listing-switches)
    (setq dired-actual-switches my-dired-hide-hidden-switches))
  ;; (message "Show hidden files: %s" (if (eq my-dired-show-hidden t) "t" "nil"))
  (revert-buffer))

(add-hook 'dired-mode-hook
          (lambda () (local-set-key (kbd "h") 'my/dired-toggle-hidden-files-view)))

;; View and change OBA keystore / truststore

(defvar my-oba-keystores '("opsa-keystore.jks" "opsa-truststore.jks" "cacerts"))
(defun my/oba-keystore (&optional pwd)
  "Open OBA keystore using default passwords. Apply custom password when using C-u."
  (interactive "P")
  (let ((kstore (ivy-completing-read "Keystore: " my-oba-keystores))
        (store-pwd)
        (full-kstore))
    (if pwd
        (setf store-pwd (read-from-minibuffer "Keystore password: "))
      (if (string= kstore "cacerts")
          (setf store-pwd "changeit")
        (setf store-pwd "keystore_neutron_analytics_bigdata_opsa_2013")))
    (if (string= kstore "cacerts")
        (setf fname (concatenate 'string "/opt/HP/opsa/jdk/jre/lib/security/" kstore) )
      (setf fname (concatenate 'string "/opt/HP/opsa/conf/ssl/" kstore) ))
    (keystore-visit fname store-pwd)))

(global-set-key (kbd "C-c k k") 'my/oba-keystore)

;; x509
;;;;;;;;;

;; Show the SHA256 fingerprint as well
;; (setq x509-x509-default-arg "x509 -text -noout -nameopt utf8 -nameopt multiline -fingerprint -sha256")

;; Alias
;;;;;;;;;;

(defalias 'esh 'eshell)
(defalias 'sl 'sort-lines)
(defalias 'lp 'list-packages)
(defalias 'prc 'package-refresh-contents)

;; Certificate stuff
(defalias 'kv 'keystore-visit)
(defalias 'vc 'x509-viewcert)


;; Keys
;;;;;;;;;

;; Misc
(global-set-key [(M-S-up)] 'my/move-line-up)
(global-set-key [(M-S-down)] 'my/move-line-down)

(global-set-key (kbd "C-a") 'my/bol-or-indent)
(global-set-key [home] 'my/bol-or-indent)
(global-set-key [end] 'end-of-line)

(global-set-key (kbd "M-g M-g") 'goto-line)

(global-set-key (kbd "C-/") 'comment-region)
(global-set-key (kbd "C-;") 'my/toggle-comment-on-line)

(global-set-key (kbd "C-S-o") 'my/insert-line-before)

(global-set-key (kbd "C-c d") 'my/duplicate-current-line)
(global-set-key (kbd "C-c r") 'my/rename-current-buffer-file)
(global-set-key (kbd "C-c f") 'find-file-at-point)

(global-set-key (kbd "C-c i") 'counsel-imenu)
(global-set-key (kbd "C-x C-b") 'ibuffer)

;; Searching
(global-set-key (kbd "C-S-s") 'isearch-forward-regexp)
(global-set-key (kbd "C-S-r") 'isearch-backward-regexp)

;; Windows
(global-set-key (kbd "C-c C-<left>")  'windmove-left)
(global-set-key (kbd "C-c C-<right>") 'windmove-right)
(global-set-key (kbd "C-c C-<up>")    'windmove-up)
(global-set-key (kbd "C-c C-<down>")  'windmove-down)

(global-set-key (kbd "C-c <left>")  'windmove-left)
(global-set-key (kbd "C-c <right>") 'windmove-right)
(global-set-key (kbd "C-c <up>")    'windmove-up)
(global-set-key (kbd "C-c <down>")  'windmove-down)

;; Switch buffers
(global-set-key (kbd "C-o") 'mode-line-other-buffer)

;; I never use overwrite mode
(global-set-key (kbd "<insert>") 'scroll-lock-mode)
(global-set-key (kbd "<Scroll_Lock>") 'scroll-lock-mode)

;; Open specific files
;;(global-set-key (kbd "C-c m") (lambda () (interactive) (find-file "~/Projects/Org/menu.org")))
(global-set-key (kbd "C-c m") 'auto-fill-mode)

;; Docker, Kubernetes
;; (global-set-key (kbd "C-c D") #'docker)
;; (global-set-key (kbd "C-c K") #'kubel)

;; Shell
(global-set-key (kbd "C-x t") 'shell-pop)

;; Swiper, Ivy, Counsel
;;;;;;;;;;;;;;;;;;;;;;;;;

;; Swiper
(global-set-key (kbd "C-s") 'swiper)
(global-set-key (kbd "C-c s") 'isearch-forward)
(define-key isearch-mode-map (kbd "M-i") 'swiper-from-isearch) ;; Does not work :(

;; Ivy & Counsel functions
;; No initial '^' for "M-x"
(global-set-key (kbd "M-x") (lambda () (interactive) (counsel-M-x "")))
(global-set-key (kbd "M-y") 'counsel-yank-pop)

(global-set-key (kbd "C-x b") 'ivy-switch-buffer)
(global-set-key (kbd "C-x B") 'counsel-recentf)
(global-set-key (kbd "C-x C-f") 'counsel-find-file)
(global-set-key (kbd "C-c C-f") 'counsel-git)

;; Org-mode functionality everywhere
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(global-set-key (kbd "C-c |") #'org-table-create-or-convert-from-region)
(global-set-key (kbd "C-c -") #'org-table-insert-hline)

;; Multiple Cursors
;;;;;;;;;;;;;;;;;;;;;

(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/unmark-next-like-this)
(global-set-key (kbd "C-M->") 'mc/skip-to-next-like-this)
(global-set-key (kbd "C-M-<") 'mc/skip-to-previous-like-this)
(global-set-key (kbd "C-S-<mouse-1>") 'mc/add-cursor-on-click)
(global-set-key (kbd "C-c C->") 'mc/mark-all-like-this)

;; Lispy functions
;;;;;;;;;;;;;;;;;;

;; IMPORTANT: These keys confuse the terminal

;; (global-set-key (kbd "M-(")  #'lispy-wrap-round)
;; (global-set-key (kbd "M-[")  #'lispy-wrap-brackets)
;; (global-set-key (kbd "M-{")  #'lispy-wrap-braces)
;; (global-set-key (kbd "M-\"") #'lispy-meta-doublequote)
;;
;; (global-set-key (kbd "C-)") #'lispy-forward-slurp-sexp)
;; (global-set-key (kbd "C-}") #'lispy-forward-barf-sexp)
;; (global-set-key (kbd "C-(") #'lispy-backward-slurp-sexp)
;; (global-set-key (kbd "C-{") #'lispy-backward-barf-sexp)

;; Function keys
;;;;;;;;;;;;;;;;;;

(global-set-key [f5] 'revert-buffer)
(global-set-key [f6] 'start-kbd-macro)
(global-set-key [f7] 'end-kbd-macro)
(global-set-key [f8] 'call-last-kbd-macro)

(global-set-key [f9] 'display-line-numbers-mode)
(global-set-key [f10] 'toggle-truncate-lines)
(global-set-key [(meta f11)] 'shrink-window-horizontally)
(global-set-key [(shift meta f11)] 'shrink-window)
(global-set-key [(meta f12)] 'enlarge-window-horizontally)
(global-set-key [(shift meta f12)] 'enlarge-window)


;; Misc
;;;;;;;;;

(setq shell-file-name "/bin/bash")

;; EOF
