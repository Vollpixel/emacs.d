;; Settings for calc

;; Add units according to https://www.remcycles.net/blog/calc.html

;; (setq calc-display-trail nil)

(setq math-additional-units
      '((GiB "1024 * MiB" "Giga Byte")
        (MiB "1024 * KiB" "Mega Byte")
        (KiB "1024 * B" "Kilo Byte")
        (B nil "Byte")
        (Gib "1024 * Mib" "Giga Bit")
        (Mib "1024 * Kib" "Mega Bit")
        (Kib "1024 * b" "Kilo Bit")
        (b "B / 8" "Bit")))

;; (setq math-units-table nil) ;; Clear the units table cache
