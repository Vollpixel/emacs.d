#!/usr/bin/bash

# Configure
./configure --prefix=/opt/my-emacs-30.1 --with-imagemagick --without-toolkit-scroll-bars

retval=$?

# Make
if [ "$retval" = "0" ]; then
    make clean
    make -j 6
fi
