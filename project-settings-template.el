;;;;
;; ~/emacs.d/project-settings-template.el
;;
;; Project settings for Emacs. This defines session specific settings for projects.
;; Such settings are environment variables or project specific language settings.
;;
;; Make a copy of this file to ~/.emacs.d/project-settings.el and apply your settings.
;;;;

;; Example Project
;;;;;;;;;;;;;;;;;;;;

(if (string= my-use-config "example-project")
    (progn
      (setenv "GOPATH" "/data/git-src/aiops-correlation/itom-analytics-opsbridge-notification/app")
      (setq go-guru-scope "github.houston.softwaregrp.net/My.Name/example-project/...")
      (message "Applied settings for Example Project")))
