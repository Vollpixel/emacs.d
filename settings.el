(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(TeX-engine 'luatex)
 '(ansi-color-faces-vector
   [default bold shadow italic underline bold bold-italic bold])
 '(ansi-color-names-vector
   ["#2E3440" "#BF616A" "#A3BE8C" "#EBCB8B" "#81A1C1" "#B48EAD" "#88C0D0" "#ECEFF4"])
 '(coffee-tab-width 4)
 '(column-number-mode t)
 '(custom-safe-themes
   '("3c83b3676d796422704082049fc38b6966bcad960f896669dfc21a7a37a748fa" default))
 '(delete-selection-mode 1)
 '(desktop-save-mode 1)
 '(display-time-mode t)
 '(fci-rule-color "#4C566A")
 '(fill-column 92)
 '(git-gutter:added-sign "▌")
 '(git-gutter:deleted-sign "▬")
 '(git-gutter:handled-backends '(git))
 '(git-gutter:modified-sign "▌")
 '(helm-buffer-max-length nil)
 '(highlight-symbol-colors
   '("#ebcb8b" "#a3be8c" "#d08770" "#b48ead" "#ffe7a6" "#c4e0ac" "#e28088" "#d5aece"
     "OliveDrab" "snow"))
 '(hl-sexp-background-color "#1e292f")
 '(inhibit-startup-screen t)
 '(jdecomp-decompiler-paths
   '((cfr . "~/bin/cfr.jar")
     (fernflower . "/opt/idea-IU/plugins/java-decompiler/lib/java-decompiler.jar")
     (procyon . "/tmp/procyon-decompiler-0.5.30.jar")))
 '(jdecomp-decompiler-type 'cfr)
 '(jdee-db-active-breakpoint-face-colors (cons "#1B2229" "#51afef"))
 '(jdee-db-requested-breakpoint-face-colors (cons "#1B2229" "#98be65"))
 '(jdee-db-spec-breakpoint-face-colors (cons "#1B2229" "#3f444a"))
 '(magit-cherry-pick-arguments '("-x"))
 '(magit-rebase-arguments '("--preserve-merges"))
 '(mark-even-if-inactive t)
 '(mouse-wheel-follow-mouse t)
 '(mouse-wheel-progressive-speed nil)
 '(org-safe-remote-resources '("\\`https://fniessen\\.github\\.io\\(?:/\\|\\'\\)"))
 '(package-selected-packages
   '(elfeed docker dockerfile-mode x509-mode engine-mode restclient command-log-mode
            powerthesaurus leo keystore-mode helpful git-gutter latex-extra auctex
            org-tree-slide org-present ob-sql-mode ob-http ob-go ox-reveal ox-gfm ox-hugo
            ox-twbs olivetti org-bullets geiser-guile geiser sly web-mode sql-indent
            flycheck-rust rustic lsp-pyright kotlin-mode jdecomp lsp-haskell haskell-mode
            groovy-mode go-errcheck gotest go-projectile go-mode forth-mode cider
            clojure-mode nasm-mode lsp-treemacs lsp-ui lsp-mode flycheck mozc shell-pop eros
            rainbow-delimiters rainbow-mode doom-modeline nord-theme spacegray-theme
            flatland-theme doom-themes diredfl dired-git-info dired-filter dired-subtree
            dired-collapse dumb-jump neotree treemacs-magit treemacs-projectile treemacs
            eyebrowse imenu-list ibuffer-vc all-the-icons-ibuffer which-key rg ivy-rich
            counsel-projectile counsel company yasnippet-snippets yasnippet undo-tree
            multiple-cursors highlight-symbol aggressive-indent move-text expand-region
            lispy indent-bars format-all gnuplot yaml-mode markdown-mode log4j-mode
            json-mode ini-mode csv-mode))
 '(pdf-view-midnight-colors (cons "#bbc2cf" "#282c34"))
 '(rustic-ansi-faces
   ["#282c34" "#ff6c6b" "#98be65" "#ECBE7B" "#51afef" "#c678dd" "#46D9FF" "#bbc2cf"])
 '(safe-local-variable-values
   '((TeX-engine . lualatex) (hl-sexp-mode) (rainbow-mode . t)))
 '(shell-pop-autocd-to-working-dir nil)
 '(shell-pop-cleanup-buffer-at-process-exit t)
 '(shell-pop-full-span t)
 '(shell-pop-shell-type '("eshell" "*eshell*" (lambda nil (eshell))))
 '(shell-pop-term-shell my-favorite-shell)
 '(shell-pop-window-position "bottom")
 '(shell-pop-window-size 40)
 '(show-paren-mode t)
 '(show-trailing-whitespace t)
 '(size-indication-mode t)
 '(transient-mark-mode 1)
 '(vc-annotate-background "#2E3440")
 '(vc-annotate-color-map
   (list (cons 20 "#A3BE8C") (cons 40 "#bbc28b") (cons 60 "#d3c68b") (cons 80 "#EBCB8B")
         (cons 100 "#e2b482") (cons 120 "#d99d79") (cons 140 "#D08770") (cons 160 "#c68984")
         (cons 180 "#bd8b98") (cons 200 "#B48EAD") (cons 220 "#b77f96") (cons 240 "#bb7080")
         (cons 260 "#BF616A") (cons 280 "#a05b67") (cons 300 "#815664") (cons 320 "#625161")
         (cons 340 "#4C566A") (cons 360 "#4C566A")))
 '(vc-annotate-very-old-color nil)
 '(warning-suppress-types '((emacs) (ox-latex))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
