;;;;
;; .emacs.d/init.el
;;
;; (c) 1991-2019 Peter Stoldt
;;;;

;;;;
;; Required external software
;;
;; - Silversearcher  counsel-ag:
;; - Ripgrep         counsel-rg: https://github.com/BurntSushi/ripgrep
;;                               https://oremacs.com/2017/08/04/ripgrep/
;; - Python          elpy:       Check list at https://github.com/jorgenschaefer/elpy
;;
;; Possible imporvements
;;
;; - https://github.com/howardabrams/dot-files
;; - https://github.com/purcell/emacs.d
;; - http://tychoish.com/post/running-multiple-emacs-daemons-on-a-single-system/
;; - https://github.com/nilcons/emacs-use-package-fast
;;
;; Emacs on Windows
;;
;; - https://gaballench.wordpress.com/2018/11/10/emacs-as-an-operating-system
;;
;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Sessions (desktop) & Local Config
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Detect current session
(defvar my-use-config (getenv "EMACS_ENV")
  "Name of the used configuration.")
(if (eq my-use-config nil)
    (setq my-use-config "default"))

;; Create the session directory
(defvar my-session-dir (concat (getenv "HOME") "/.emacs.d/.s-" my-use-config "/")
  "Sub-directory under ~/.emacs.d for environment specific configuration files.")
(unless (file-exists-p my-session-dir)
  (make-directory my-session-dir))

;; Change location where the destop file is expected
(defvar desktop-path (list my-session-dir)
  "Specifies the desktop path specific to an environment.")

;; Change location of the bookmarks file
(defvar bookmark-default-file (concat my-session-dir "bookmarks"))

;; Change location of the projectile cache file
(defvar projectile-cache-file (concat my-session-dir "projectile.cache"))

;; Load local settings that differ between systems. These are for example
;; user-name, mail-address, url-proxy or exec-path
(let ((my-local-settings-file "~/.emacs.d/local-settings.el"))
  (when (file-exists-p my-local-settings-file)
    (load-file my-local-settings-file)))

;; Set the window title for editor identification in the UI
(defvar my-title-format (upcase (concat "[" my-use-config "] - %f")))

;; Decrease start-up time by adjusting garbage collection thresholds
;; during startup and thereafter
(setq gc-cons-threshold (* 256 1024 1024))

;; The following does not reset the value
;; (let ((normal-gc-cons-threshold (* 128 1024 1024))
;;       (init-gc-cons-threshold (* 256 1024 1024)))
;;   (setq gc-cons-threshold init-gc-cons-threshold)
;;   (add-hook 'emacs-startup-hook
;;             (lambda () (setq gc-cons-threshold normal-gc-cons-threshold))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Packages
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Define package repositories
(require 'package)

;; Package Archives

(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)
;; (add-to-list 'package-archives '("local-elpa" . "~/.emacs.d/local-elpa/") t)

;; (add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/"))
;; (add-to-list 'package-archives '("gnu" . "https://elpa.gnu.org/packages/") t)

;; Load and activate emacs packages
(package-initialize)

;; Download the ELPA archive description if needed
(when (not package-archive-contents)
  (package-refresh-contents))

;; Install wanted packages. These can also be installed with M-x package-install
(defvar my-packages
  '(
    ;; Minor Modes
    ;;;;;;;;;;;;;;;;

    csv-mode            ;; No reference - https://elpa.gnu.org/packages/csv-mode.html
    ;; emmet-mode       ;; https://github.com/smihica/emmet-mode
    ;; gnuplot-mode        ;; https://github.com/mkmcc/gnuplot-mode
    gnuplot             ;; https://github.com/bruceravel/gnuplot-mode
    graphviz-dot-mode   ;; http://www.graphviz.org
    groovy-mode         ;; https://github.com/Groovy-Emacs-Modes/groovy-emacs-modes
    ;; hy-mode             ;; https://github.com/hylang/hy-mode
    ini-mode            ;; https://github.com/Lindydancer/ini-mode
    json-mode
    kotlin-mode         ;; https://github.com/Emacs-Kotlin-Mode-Maintainers/kotlin-mode
    markdown-mode       ;; http://jblevins.org/projects/markdown-mode
    ;; mediawiki           ;; https://github.com/hexmode/mediawiki-el
    sql-indent          ;; https://github.com/bsvingen/sql-indent
    ;; sqlformat           ;; https://github.com/purcell/sqlformat
    ;; tql-mode            ;; https://github.com/tiros-dev/tql-mode
    web-mode            ;; http://web-mode.org/
    yaml-mode           ;; https://github.com/yoshiki/yaml-mode

    ;; Structural editing
    ;;;;;;;;;;;;;;;;;;;;;;;

    ;; Alternatives for paredit:
    ;; Lispy    - https://github.com/abo-abo/lispy
    ;; Parinfer - https://shaunlebron.github.io/parinfer

    ;; Paredit cheatsheet: http://www.emacswiki.org/emacs/PareditCheatsheet
    paredit             ;; https://www.emacswiki.org/emacs/ParEdit
    expand-region       ;; https://github.com/magnars/expand-region.el
    move-text           ;; https://github.com/emacsfodder/move-text
    aggressive-indent   ;; https://github.com/Malabarba/aggressive-indent-mode
    highlight-symbol    ;; https://github.com/nschum/highlight-symbol.el
    iedit               ;; Iedit allows you to edit the occurrence text in a buffer simultaneously
    multiple-cursors    ;; https://github.com/magnars/multiple-cursors.el
    yasnippet           ;; https://github.com/joaotavora/yasnippet
    yasnippet-snippets  ;; https://github.com/AndreaCrotti/yasnippet-snippets
    company             ;; http://company-mode.github.io
    undo-tree

    ;; Navigation
    ;;;;;;;;;;;;;;;

    ;; Check also:
    ;;   https://github.com/abo-abo/hydra/blob/master/hydra-examples.el
    ;;   https://dfeich.github.io/www/org-mode/emacs/2018/05/10/context-hydra.html

    projectile          ;; https://github.com/bbatsov/projectile
    ibuffer-vc          ;; https://github.com/purcell/ibuffer-vc
    ;; ibuffer-projectile  ;; https://github.com/purcell/ibuffer-projectile
    ;; perspective-el      ;; https://github.com/nex3/perspective-el

    hydra               ;; https://github.com/abo-abo/hydra
    ivy
    swiper              ;; https://github.com/abo-abo/swiper
    counsel
    counsel-projectile
    ;; ivy-hydra
    ivy-yasnippet       ;; https://github.com/mkcms/ivy-yasnippet
    ;; ivy-dired-history   ;; https://github.com/jixiuf/ivy-dired-history
    ;; openwith            ;; https://www.emacswiki.org/emacs/OpenWith
    ace-window          ;; https://github.com/abo-abo/ace-window
    eyebrowse           ;; https://github.com/wasamasa/eyebrowse
    which-key           ;; https://github.com/justbur/emacs-which-key
    dumb-jump           ;; https://github.com/jacktasia/dumb-jump

    ;; https://fuco1.github.io/2017-07-15-Collapse-unique-nested-paths-in-dired-with-dired-collapse-mode.html
    ;; https://github.com/Fuco1/dired-hacks
    ;; dired-rainbow
    dired-recent        ;; https://github.com/vifon/dired-recent.el
    dired-narrow
    dired-collapse
    dired-subtree

    ;; UI
    ;;;;;;;

    beacon              ;; Highlight line at cursor
    rainbow-mode        ;; Show colors inline
    rainbow-delimiters

    ;; golden-ratio        ;; https://github.com/roman/golden-ratio.el

    ;; Use a smarter mode-line
    smart-mode-line     ;; https://github.com/Malabarba/smart-mode-line
    smart-mode-line-powerline-theme

    ;; Alternatives:
    ;;    https://github.com/seagle0128/doom-modeline
    ;;    https://github.com/dbordak/telephone-line
    ;;    https://github.com/tarsius/moody + https://github.com/tarsius/minions
    ;;    https://github.com/TheBB/spaceline

    ;; 日本語
    mozc                ;; Check settings in my-editing.el

    ;; Programming Language Support
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;; https://github.com/emacs-lsp/dap-mode   Debug Adapter Protocol is a wire protocol for communication between
    ;;                                         client and Debug Server might be an alternative for go-dlv
    ;; https://github.com/emacs-lsp/lsp-mode   Language Server Protocol Support for Emacs
    ;; https://github.com/emacs-lsp/lsp-ui     Contains all the higher level UI modules of lsp-mode, like flycheck
    ;;                                         support and code lenses
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;; https://www.mortens.dev/blog/emacs-and-the-language-server-protocol/
    ;; https://www.reddit.com/r/emacs/comments/931la6/tip_how_to_adopt_flycheck_as_your_new_best_friend/
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    flycheck            ;; http://www.flycheck.org/en/latest/
    ;; flycheck-gradle
    ;; https://github.com/flycheck/flycheck-color-mode-line

    ;; Clojure
    ;;;;;;;;;;;;

    ;; Key bindings and code colorization for Clojure:
    clojure-mode        ;; https://github.com/clojure-emacs/clojure-mode
    cider               ;; https://github.com/clojure-emacs/cider

    ;; clj-refacor temporarily disabled
    ;; clj-refactor        ;; https://github.com/clojure-emacs/clj-refactor.el
    ;; flycheck-clojure    ;; https://github.com/clojure-emacs/squiggly-clojure

    ;; Lisp
    ;;;;;;;;;

    slime               ;; https://github.com/slime/slime
    ;;                  ;; https://racket-lang.org
    ;; racket-mode         ;; https://github.com/greghendershott/racket-mode

    ;; Go
    ;;;;;;;

    ;; https://www.emacswiki.org/emacs/GoLangMode
    ;; http://dominik.honnef.co/posts/2013/03/writing_go_in_emacs/
    ;; https://www.simplify.ba/articles/2016/03/04/go-development-with-gnu-emacs/
    ;; https://www.youtube.com/watch?v=r6j2W5DZRtA

    go-mode             ;; https://github.com/dominikh/go-mode.el
    go-errcheck         ;; https://github.com/dominikh/go-errcheck.el
    go-projectile       ;; https://github.com/dougm/go-projectile
    go-eldoc            ;; https://github.com/syohex/emacs-go-eldoc
    go-dlv              ;; https://github.com/benma/go-dlv.el
    ;;                  ;; https://github.com/emacs-lsp/dap-mode
    go-imenu            ;; https://github.com/brantou/go-imenu.el
    gotest              ;; https://github.com/nlamirault/gotest.el
    company-go          ;; https://github.com/nsf/gocode/tree/master/emacs-company

    ;; http://gorefactor.org/index.html
    godoctor            ;; https://github.com/emacsmirror/godoctor

    ;; https://github.com/nsf/gocode
    ;; https://github.com/manute/gorepl-mode
    ;; https://github.com/grafov/go-playground
    ;; https://github.com/favadi/flycheck-gometalinter

    ;; Rust
    ;;;;;;;;;

    ;; rust-mode           ;; https://github.com/rust-lang/rust-mode

    ;; Python
    ;;;;;;;;;;;

    ;; https://elpy.readthedocs.io/en/latest/index.html
    elpy                ;; https://github.com/jorgenschaefer/elpy3
    pydoc               ;; https://github.com/statmobile/pydoc

    ;; https://github.com/proofit404/anaconda-mode
    ;; https://github.com/proofit404/company-anaconda
    ;; https://github.com/abo-abo/lpy

    ;; C/C++
    ;;;;;;;;;;

    ;; https://github.com/atilaneves/cmake-ide
    ;; http://martinsosic.com/development/emacs/2017/12/09/emacs-cpp-ide.html
    ;; https://github.com/leoliu/ggtags

    ;; Assembly
    ;;;;;;;;;;;;;

    ;; https://www.emacswiki.org/emacs/AssemblyProgramming
    ;; https://github.com/skeeto/nasm-mode/blob/master/nasm-mode.el
    ;; There is also gas-mode.el in the vendor directory
    ;;   - http://www.hczim.de/software/gas-mode.html

    ;; Java
    ;;;;;;;;;

    ;; https://github.com/emacs-lsp/lsp-java
    ;; http://mp.vv.si/blog/emacs/a-maven-plugin-for-emacs/
    ;; https://github.com/mopemope/meghanada-emacs
    ;; http://eclim.org + https://github.com/senny/emacs-eclim
    ;; https://github.com/dakrone/emacs-java-imports/
    ;; https://github.com/leoliu/ggtags

    ;; lsp-intellij:
    ;; https://github.com/Ruin0x11/lsp-intellij
    ;; https://github.com/Ruin0x11/intellij-lsp-server

    ;; JavaScript
    ;;;;;;;;;;;;;;;

    ;; https://github.com/ananthakumaran/tide
    ;; See also: https://hotair.tech/blog/goodbye-vscode/

    ;; https://github.com/NicolasPetton/Indium
    ;; https://indium.readthedocs.io/en/latest/
    ;; https://emacs.cafe/emacs/javascript/setup/2017/04/23/emacs-setup-javascript.html
    ;; https://emacs.cafe/indium/emacs/javascript/2017/07/24/indium-0.7-released.html
    ;; https://github.com/mooz/js2-mode
    ;; https://github.com/magnars/js2-refactor.el
    ;; https://github.com/nicolaspetton/xref-js2

    ;; Org-mode
    ;;;;;;;;;;;;;

    ;; NOTE: Some export modules fail sometimes with a nasty error.
    ;;       If that happens, either of these will help:
    ;;
    ;;       - Recompile all .elc files in elpa/org-xxxx directory: "C-0 M-x byte-compile-directory"
    ;;       - If that doesn't help, reload org-mode: "C-u M-x org-reload"

    org                 ;; Load the most recent version, don't rely on the embedded version
    org-bullets
    htmlize

    ;; Export modules
    ox-gfm              ;; https://github.com/larstvei/ox-gfm (github markdown export)
    ox-pandoc           ;; https://github.com/kawabata/ox-pandoc
    ;; ox-reveal           ;; https://github.com/yjwen/org-reveal ;; Not working since Org 9.2
    ox-rst              ;; https://github.com/masayuko/ox-rst
    ox-hugo             ;; https://ox-hugo.scripter.co/  ;; https://gohugo.io/

    ;; Org-babel modules
    ob-go               ;; https://github.com/pope/ob-go

    ;; Simple presentation mode for Emacs Org-mode
    ;; epresent            ;; https://github.com/eschulte/epresent

    ;; https://github.com/takaxp/org-tree-slide

    ;; LaTeX
    ;;;;;;;;;;

    auctex              ;; https://www.gnu.org/software/auctex
    company-auctex
    latex-extra         ;; https://github.com/Malabarba/latex-extra
    ;; pdf-tools           ;; https://github.com/politza/pdf-tools
    ;;                  ;; http://mactex-wiki.tug.org/wiki/index.php/SyncTeX
    ;;                  acronyms: https://florian.adamsky.it/2018/03/09/emacs-add-acronyms.html

    ;; Git
    ;;;;;;;;

    magit               ;; https://github.com/magit/magit
    git-gutter
    ;;                  ;; https://github.com/charignon/github-review

    ;; Tools
    ;;;;;;;;;;

    rg                  ;; https://github.com/dajva/rg.el
    helpful             ;; https://github.com/Wilfred/helpful
    powerthesaurus      ;; https://github.com/SavchenkoValeriy/emacs-powerthesaurus
    shell-pop           ;; https://github.com/kyagi/shell-pop-el
    ;; better-shell        ;; https://github.com/killdash9/better-shell
    ;; restclient          ;; https://github.com/pashky/restclient.el
    vdiff               ;; https://github.com/justbur/emacs-vdiff
    ;; vdiff-magit         ;; https://github.com/justbur/emacs-vdiff-magit
    command-log-mode    ;; https://github.com/lewang/command-log-mode

    ;; Search the web with engine-mode. Config in navigation.el
    engine-mode         ;; https://github.com/hrs/engine-mode

    ;; StackExchange for Emacs
    sx                  ;; https://github.com/vermiculus/sx.el

    ;; sidebar is a customizable file explorer with git integration
    ;; https://github.com/sebastiencs/sidebar.el
    neotree             ;; https://github.com/jaypei/emacs-neotree

    ;; keystore-mode       ;; https://github.com/peterpaul/keystore-mode for Java keystores

    ;; Database
    ;;;;;;;;;;;;;

    ;; http://john.mercouris.online/emacs-database-interface.html

    ;; Docker
    ;;;;;;;;;;;

    dockerfile-mode     ;; https://github.com/spotify/dockerfile-mode
    docker              ;; https://github.com/Silex/docker.el
    docker-tramp        ;; https://github.com/emacs-pe/docker-tramp.el
    ;; Use of docker-tramp:
    ;;    /docker:user@container_id:/path/to/file

    ;; http://manuel-uberti.github.io/emacs/2017/10/19/docker/
    ;; https://github.com/meqif/docker-compose-mode
    ;; https://github.com/Fuco1/eshell-bookmark

    ;; Kubernetes
    ;;;;;;;;;;;;;;;

    ;; kubernetes          ;; https://github.com/chrisbarrett/kubernetes-el
    ;; k8s-mode            ;; https://github.com/TxGVNN/emacs-k8s-mode

    ;; Mail
    ;;;;;;;;;

    ;; Note: mu4e.el comes not as package from elpa, but is part of the mu package
    ;;       Check my-mu4e.el

    ;; Themes
    ;;;;;;;;;;;

    material-theme

    ;; NOTE: because of a problem with some functionality in org-mode,
    ;;       it was necessary to fix material theme. The fixed version
    ;;       is stored in .emacs.d/themes/myterial-theme.el and loaded
    ;;       from there.

    ;; color-theme-sanityinc-tomorrow

    ;; Other nice themes:
    ;; arjen-grey-theme, atom-one-dark-theme, afternoon-theme, spacegray-theme,
    ;; underwater-theme, smyx-theme, aurora-theme, mustard-theme, tangotango-theme,
    ;; leuven-theme, white-sand-theme, hc-zenburn-theme, solarized-theme,
    ;; idea-darkula-theme

    ;; Warning: Don't use moe-theme - It applies unwanted changes in config files
    ;;          that are not owned by the package

    ;; https://pawelbx.github.io/emacs-theme-gallery/
    ;; https://github.com/purcell/color-theme-sanityinc-tomorrow
    ;; http://themegallery.robdor.com/

    ))

;; Install packages
(dolist (p my-packages)
  (when (not (package-installed-p p))
    (package-install p)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Emacs Customization
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Place downloaded elisp files in ~/.emacs.d/vendor to load them
(add-to-list 'load-path "~/.emacs.d/vendor")

;; Add customization directory to load path
(add-to-list 'load-path "~/.emacs.d/customizations")

;; Path to special or changed themes
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")
(add-to-list 'load-path "~/.emacs.d/themes")

;; NOTE: All custom files have the prefix 'my' to make sure, that the
;;       correct file will be loaded and not one from another package

;; Sets up exec-path-from-shell so that Emacs will use the correct
;; environment variables
(load "my-shenv.el")

;; Load settings first
(load "my-settings.el")

;; My custom functions
(load "my-functions.el")

;; Simplify the navigation and configure recentf, restclient, ...
(load "my-navigation.el")
(load "my-projectile.el")
(load "my-hydra.el")
(load "my-ivy.el")
(load "my-tools.el")

;; Specify the look of Emacs
(load "my-theme.el")
(load "my-ui.el")
(load "my-modeline.el")

;; Modes
(load "my-web.el")
(load "my-sql.el")

;; Tools setup
(load "my-git.el")
(load "my-yasnippet.el")
(load "my-diff.el")
(load "my-eshell.el")

;; Make editing a bit nicer
(load "my-editing.el")
(load "my-company.el")

;; Langauage specific setup
(load "my-lisp.el")
(load "my-python.el")
(load "my-js.el")
(load "my-golang.el")
;;(load "my-cpp.el")

;; Text modes
(load "my-latex.el")
(load "my-rst.el")
(load "my-orgmode.el")

;; Tools
;; (load "my-mu4e.el")

;; Note: Keys should always be the last files to load
(load "my-keys.el")

;; Reset GC threshold to a smaller value after config loading
;; Emacs default is 800000 - I set it to 1 MiB
(setq gc-cons-threshold (* 1 1024 1024))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Project Customization
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Load session specific settings
(load "my-session-settings.el")

;; Set the window title
(setq frame-title-format my-title-format)

;; Desktop is strange: Explicitely read desktop only when the config
;; is not "default", otherwise it does not work correctly
(unless (string= my-use-config "default")
  (desktop-read))

;;; init.el ends here
