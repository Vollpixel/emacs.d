#!/bin/sh

version="1.1"

calElisp="(progn
  (when (display-graphic-p)
    (tool-bar-mode -1)
    (scroll-bar-mode -1))
  (menu-bar-mode -1)
  (blink-cursor-mode -1)

  (setq
     select-enable-primary t
     help-window-select t)

  (calendar)
  (delete-other-windows)

  (define-key calendar-mode-map [?q]
    (quote save-buffers-kill-terminal)))"

geometry="-g 636x188+970+520"
config="-q -l ~/.emacs.d/init-app.el"

#emacs -Q -nw --eval="$calElisp"
emacs $config -nw --eval="$calElisp"
