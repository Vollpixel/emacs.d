;;;;
;; (c) 1991-2023 Peter Stoldt
;;
;;     32 Years of Emacs
;;;;

;; Changes all yes/no questions to y/n type
(fset 'yes-or-no-p 'y-or-n-p)

;; Emacs server:
;;   emacs --daemon
;;   emacsclient -c [-a emacs]
;;(server-start)

;; HTTPS proxy settings that differ between systems.
(let ((my-proxy-settings-file "~/.emacs.d/proxy-settings.el"))
  (when (file-exists-p my-proxy-settings-file)
    (load-file my-proxy-settings-file)))

;; Define package repositories
(require 'package)

(setq package-enable-at-startup nil)

;; Package Archives
(setq package-archives
      '(("gnu" . "https://elpa.gnu.org/packages/")
        ("melpa" . "https://melpa.org/packages/")
        ("melpa-stable" . "https://stable.melpa.org/packages/")
        ("nongnu" . "https://elpa.nongnu.org/nongnu/")))

;; Load and activate emacs packages
(package-initialize)

;; Bootstrap `use-package'
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; Load my configuration
(org-babel-load-file (expand-file-name "~/.emacs.d/config.org"))
