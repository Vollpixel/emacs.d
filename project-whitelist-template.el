;;;;
;; ~/emacs.d/project-whitelist-template.el
;;
;; Define the list of Java projects that should use a Java LSP
;;
;; Make a copy of this file to ~/.emacs.d/project-whitelist.el
;; and apply your settings.
;;;;

;; Enable Java LSP for the projects in my-project-whitelist
(setq my-project-whitelist
      (list
       "session1"
       "session2"
       ))
