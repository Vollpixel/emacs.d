;;;;
;;          init-23.1.el
;; Emacs configuration for old Emacs 32.1 of CentOS 6
;;;;

;; Customization files
;;;;;;;;;;;;;;;;;;;;;;;;

;; Some custom settings and directories:

(add-to-list 'load-path "~/.emacs.d/vendor")


;; UI
;;;;;;;

;; Cursor
(blink-cursor-mode -1)
(set-cursor-color "#ffe7a6")

;; Keep syntax highlighting
(set-face-foreground 'highlight nil)

;;(global-linum-mode)
;;(setq linum-format "%4d\u2502")

(when (fboundp 'tool-bar-mode)
  (tool-bar-mode -1))

(menu-bar-mode -1)


;; Custom variables
;;;;;;;;;;;;;;;;;;;;;

;; Custom variables are also defined in ~/.emacs.d/settings.el
(setq custom-file "~/.emacs.d/settings.el")

(if (file-exists-p custom-file)
    (load custom-file))

;; Changes all yes/no questions to y/n type
(fset 'yes-or-no-p 'y-or-n-p)

;; Use UTF-8 as default encoding
(prefer-coding-system 'utf-8)
(setq-default buffer-file-coding-system 'utf-8-unix)

;; Jump into help window
(setq help-window-select t)

(setq make-backup-files nil)
(setq auto-save-default nil)

;; Warn only for really large files
(setq large-file-warning-threshold 150000000)
(setq create-lockfiles nil)
(setq inhibit-startup-message t)
(setq ring-bell-function 'ignore)

;; Start scrolling slightly before end of window
(setq scroll-margin 4
      scroll-step 1
      scroll-conservatively 10000)

;; Newline at end of file
(setq require-final-newline t)

;; Enable commands to change case (C-x C-u, C-x C-l)
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

;; OS interaction
(setq  save-interprogram-paste-before-kill t
       apropos-do-all t
       mouse-yank-at-point t)

(add-hook 'after-save-hook
          'executable-make-buffer-file-executable-if-script-p)


;; Indentation
;;;;;;;;;;;;;;;;

(setq tab-width 4)

;; Use Tab to Indent or Complete
(setq tab-always-indent 'complete)

;; Don't use hard tabs
(setq-default indent-tabs-mode nil)

;; default indentation
(setq-default sh-basic-offset 4)
(setq-default sh-indentation 4)

(add-hook 'java-mode-hook
          (lambda ()
            (setq c-basic-offset 4
                  tab-width 4)))


;; Dired
;;;;;;;;;;

(if (eq system-type 'gnu/linux)
    (setq dired-listing-switches "-alh --group-directories-first"))


;; Unique buffer names
;;;;;;;;;;;;;;;;;;;;;;;;

;; Prevent identical buffer names when files have identical names
;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Uniquify.html
(require 'uniquify)
(setq uniquify-buffer-name-style 'forward)
;; from prelude: https://github.com/bbatsov/prelude
(setq uniquify-separator "/")
(setq uniquify-after-kill-buffer-p t)    ; rename after killing uniquified
(setq uniquify-ignore-buffers-re "^\\*") ; don't muck with special buffers


;; eshell
;;;;;;;;;;;

(setq eshell-prompt-regexp "^[^#$\n]*[#$] "
      eshell-prompt-function
      (lambda ()
        (concat "[" (user-login-name) "@" (system-name) " "
                (if (string= (eshell/pwd) (getenv "HOME"))
                    "~" (eshell/basename (eshell/pwd)))
                "]"
                (if (= (user-uid) 0) "# " "$ "))))


;; Diff settings
;;;;;;;;;;;;;;;;;;

;; ediff
;; (setq ediff-split-window-function 'split-window-horizontally)
(setq ediff-split-window-function 'split-window-vertically)

;; Use Emacs as diff-tool: https://www.emacswiki.org/emacs/EdiffMode
;; Usage: emacs -ediff file1 file2
(defun command-line-ediff (switch)
  (let ((file1 (pop command-line-args-left))
        (file2 (pop command-line-args-left)))
    (ediff-files file1 file2)))

(add-to-list 'command-switch-alist '("-ediff" . command-line-ediff))


;; Editing
;;;;;;;;;;;;

;; Don't save file status
(setq desktop-save-mode nil)

;; Put all backups in ~/.emacs.d/backups
;; http://www.gnu.org/software/emacs/manual/html_node/elisp/Backup-Files.html
(setq backup-directory-alist `(("." . ,(concat user-emacs-directory "backups"))))
(setq auto-save-default nil)

;; (setq electric-indent-mode nil)
(setq electric-indent-mode t)

;; Automatically replace selection
(delete-selection-mode 1)

;; Don't highlight trailing whitespaces
(custom-set-variables
 '(show-trailing-whitespace nil))


;; Functions
;;;;;;;;;;;;;;

(defun my/bol-or-indent ()
  "Move cursor to beginning of line or to indentation"
  (interactive)
  (if (bolp)
      (back-to-indentation)
    (beginning-of-line)))

;; Duplicate line under cursor (from EmacsWiki)

(defun my/duplicate-current-line (&optional n)
  "Duplicate current line, make more than 1 copy given a numeric argument."
  (interactive "p")
  (save-excursion
    (let ((nb (or n 1))
          (current-line (thing-at-point 'line)))
      ;; when on last line, insert a newline first
      (when (or (= 1 (forward-line 1)) (eq (point) (point-max)))
        (newline))
      ;; now insert as many time as requested
      (while (> n 0)
        (insert current-line)
        (decf n)))))

(defun my/move-line-up ()
  (interactive)
  (transpose-lines 1)
  (previous-line 2))

(defun my/move-line-down ()
  (interactive)
  (next-line 1)
  (transpose-lines 1)
  (previous-line 1))

;; dired

(defvar my-dired-show-hidden t)
(defvar my-dired-hide-hidden-switches "-lh --group-directories-first --time-style=long-iso")

(defun my/dired-toggle-hidden-files-view ()
  (interactive)
  (setq my-dired-show-hidden (not my-dired-show-hidden))
  (if (eq my-dired-show-hidden t)
      (setq dired-actual-switches dired-listing-switches)
    (setq dired-actual-switches my-dired-hide-hidden-switches))
  ;; (message "Show hidden files: %s" (if (eq my-dired-show-hidden t) "t" "nil"))
  (revert-buffer))

(add-hook 'dired-mode-hook
          (lambda () (local-set-key (kbd "h") 'my/dired-toggle-hidden-files-view)))

;; Alias
;;;;;;;;;;

(defalias 'esh 'eshell)
(defalias 'sl 'sort-lines)


;; Keys
;;;;;;;;;

;; Misc
(global-set-key [(M-S-up)] 'my/move-line-up)
(global-set-key [(M-S-down)] 'my/move-line-down)

(global-set-key (kbd "C-a") 'my/bol-or-indent)
(global-set-key [home] 'my/bol-or-indent)
(global-set-key [end] 'end-of-line)

(global-set-key (kbd "M-g M-g") 'goto-line)

(global-set-key (kbd "C-/") 'comment-region)
(global-set-key (kbd "C-;") 'my/toggle-comment-on-line)

(global-set-key (kbd "C-c d") 'my/duplicate-current-line)
(global-set-key (kbd "C-c r") 'my/rename-current-buffer-file)
(global-set-key (kbd "C-c f") 'find-file-at-point)

(global-set-key (kbd "C-x C-b") 'ibuffer)

;; Searching
(global-set-key (kbd "C-S-s") 'isearch-forward-regexp)
(global-set-key (kbd "C-S-r") 'isearch-backward-regexp)

;; Windows
(global-set-key (kbd "C-c C-<left>")  'windmove-left)
(global-set-key (kbd "C-c C-<right>") 'windmove-right)
(global-set-key (kbd "C-c C-<up>")    'windmove-up)
(global-set-key (kbd "C-c C-<down>")  'windmove-down)

(global-set-key (kbd "C-c <left>")  'windmove-left)
(global-set-key (kbd "C-c <right>") 'windmove-right)
(global-set-key (kbd "C-c <up>")    'windmove-up)
(global-set-key (kbd "C-c <down>")  'windmove-down)

;; I never use overwrite mode
(global-set-key (kbd "<insert>") 'scroll-lock-mode)
(global-set-key (kbd "<Scroll_Lock>") 'scroll-lock-mode)

;; Open specific files
;;(global-set-key (kbd "C-c m") (lambda () (interactive) (find-file "~/Projects/Org/menu.org")))
(global-set-key (kbd "C-c m") 'auto-fill-mode)

;; Function keys
;;;;;;;;;;;;;;;;;;

(global-set-key [f5] 'revert-buffer)
(global-set-key [f6] 'start-kbd-macro)
(global-set-key [f7] 'end-kbd-macro)
(global-set-key [f8] 'call-last-kbd-macro)

(global-set-key [f9] 'display-line-numbers-mode)
(global-set-key [f10] 'toggle-truncate-lines)
(global-set-key [(meta f11)] 'shrink-window-horizontally)
(global-set-key [(shift meta f11)] 'shrink-window)
(global-set-key [(meta f12)] 'enlarge-window-horizontally)
(global-set-key [(shift meta f12)] 'enlarge-window)


;; Misc
;;;;;;;;;

(setq shell-file-name "/bin/bash")

;; EOF
