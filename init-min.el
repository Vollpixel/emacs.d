;;;;
;;             min-init.el
;; Minimal configuration for faster load
;;;;

;; Define, which configuration this is
(setq my-use-config "bare")

;; Make it possible to identify the editor
(setq my-title-format (concat (upcase my-use-config) " - %b"))

;; Save desktop files in the session directory
(defvar my-session-dir (concat (getenv "HOME") "/.emacs.d/.s-" my-use-config "/")
  "Sub-directory under ~/.emacs.d for environment specific configuration files.")
(unless (file-exists-p my-session-dir)
  (make-directory my-session-dir))

;; Change location of the bookmarks file
(defvar bookmark-default-file (concat my-session-dir "bookmarks"))

;; HTTPS proxy settings that differ between systems.
(let ((my-proxy-settings-file "~/.emacs.d/proxy-settings.el"))
  (when (file-exists-p my-proxy-settings-file)
    (load-file my-proxy-settings-file)))


;; Packages
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Define package repositories
(require 'package)

(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)

;; Load and activate emacs packages
(package-initialize)

;; Download the ELPA archive description if needed
(when (not package-archive-contents)
  (package-refresh-contents))

;; Install wanted packages. These can also be installed with M-x package-install
(defvar my-packages
  '(
    ;; Modes
    dockerfile-mode
    forth-mode
    go-mode
    groovy-mode
    json-mode
    kotlin-mode
    markdown-mode
    yaml-mode

    ;; Modules
    lispy
    magit
    git-gutter
    highlight-symbol
    multiple-cursors
    swiper
    counsel
    rg

    ;; Dired
    dired-collapse
    dired-subtree
    dired-git-info
    diredfl

    ;; Tools
    leo
    docker
    keystore-mode
    which-key
    neotree
    shell-pop
    jdecomp
    x509-mode

    ;; UI
    smart-mode-line
    smart-mode-line-powerline-theme
    ;;doom-modeline

    ;; Themes
    ;; catppuccin-theme
    ;; arjen-grey-theme
    ;; flatland-theme
    ;; afternoon-theme
    ))

;; Install packages, that are not already installed
(dolist (p my-packages)
  (when (not (package-installed-p p))
    (package-install p)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; Customization files
;;;;;;;;;;;;;;;;;;;;;;;;

;; Some custom settings and directories:

(add-to-list 'load-path "~/.emacs.d/vendor")
(add-to-list 'load-path "~/.emacs.d/lisp")
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")
(add-to-list 'load-path "~/.emacs.d/themes")


;; Theme
;;;;;;;;;;

;; (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
;;       doom-themes-enable-italic t) ; if nil, italics is universally disabled
;; (load-theme 'doom-one t)
;; ;; Enable flashing mode-line on errors
;; (doom-themes-visual-bell-config)
;; ;; Enable custom neotree theme (all-the-icons must be installed!)
;; (doom-themes-neotree-config)
;; ;; or for treemacs users
;; (setq doom-themes-treemacs-theme "doom-colors") ; use the colorful treemacs theme
;; (doom-themes-treemacs-config)
;; ;; Corrects (and improves) org-mode's native fontification.
;; (doom-themes-org-config)

;; I know, this 'if' makes currently no sense
(if (string= "root" (getenv "USER"))
    (load-theme 'nordisch t)

  (if (or (display-graphic-p)
          (string= "xterm-24bit" (getenv "TERM")))
      (load-theme 'nordisch t)
    (load-theme 'nordisch t)))


;; Smart Modeline
;;;;;;;;;;;;;;;;;;;

;; Use theme
(require 'smart-mode-line)

;; (if (display-graphic-p)
;;     (setq sml/theme 'powerline)
;;   (setq sml/theme 'respectful))
;; respectful, dark, light
(setq sml/theme 'respectful)

;; Prevent asking for permission
(setq sml/no-confirm-load-theme t)
(sml/setup)


;; Doom Modeline
;;;;;;;;;;;;;;;;;;

;; (require 'doom-modeline)
;; (doom-modeline-mode 1)
;; (setq doom-modeline-height 20
;;       ;; doom-modeline-buffer-file-name-style 'relative-from-project
;;       doom-modeline-buffer-file-name-style 'truncate-upto-project)
;;
;; (if (eq system-type 'windows-nt)
;;     (setq doom-modeline-icon nil)
;;   (setq doom-modeline-icon t))


;; Custom variables
;;;;;;;;;;;;;;;;;;;;;

;; Define my favorite shell
(defvar my-favorite-shell "/bin/bash" "My favorite shell.")

;; Shell to use in docker or term
(setq shell-file-name my-favorite-shell)

;; NOTE: Custom variables are defined in ~/.emacs.d/settings.el
(setq custom-file "~/.emacs.d/settings.el")
;; (unless (display-graphic-p)
;;   (menu-bar-mode -1))
(menu-bar-mode -1)

(if (file-exists-p custom-file)
    (load custom-file))

;; Changes all yes/no questions to y/n type
(fset 'yes-or-no-p 'y-or-n-p)

;; Mouse wheel scrolling
(setq mouse-wheel-scroll-amount
      '(2 ((shift) . hscroll)
          ((meta))
          ((control) . text-scale)))

;; Use UTF-8 as default encoding
(prefer-coding-system 'utf-8)
(setq-default buffer-file-coding-system 'utf-8-unix)

;; Jump into help window
(setq help-window-select t)

(setq make-backup-files nil)
(setq auto-save-default nil)

;; (setq initial-scratch-message ";; Your hacking starts here!\n")
(setq initial-scratch-message "")

;; Warn only for really large files
(setq large-file-warning-threshold 150000000)

;; System PATH
(when (eq system-type 'windows-nt)
  (setenv "PATH" (concat (getenv "PATH") ";"
                         "C:\\Program Files\\Git\\usr\\bin" ";"
                         "C:\\Users\\pestoldt.CORPDOM\\bin")))

(setq create-lockfiles nil)
(setq inhibit-startup-message t)

;; NO BELL !!!
(setq ring-bell-function 'ignore)

;; Start scrolling slightly before end of window
(setq scroll-margin 3
      scroll-step 0
      scroll-conservatively 10000)

;; Newline at end of file
(setq require-final-newline t)

;; Enable commands to change case (C-x C-u, C-x C-l)
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'narrow-to-region 'disabled nil)

;; OS interaction
(setq x-select-enable-clipboard t
      x-select-enable-primary t
      save-interprogram-paste-before-kill t
      apropos-do-all t
      mouse-yank-at-point t)

(add-hook 'after-save-hook
          'executable-make-buffer-file-executable-if-script-p)

(add-hook 'prog-mode-hook 'display-line-numbers-mode)


;; Indentation
;;;;;;;;;;;;;;;;

(setq tab-width 4)

;; Use Tab to Indent or Complete
(setq tab-always-indent 'complete)

;; Don't use hard tabs
(setq-default indent-tabs-mode nil)

;; default indentation
(setq-default sh-basic-offset 4)
(setq-default sh-indentation 4)

(add-hook 'java-mode-hook
          (lambda ()
            (setq c-basic-offset 4
                  tab-width 4)))

;; nXML Extrawurst
(setq nxml-child-indent 4
      nxml-attribute-indent 4)

;; graphviz-dot-mode
(setq graphviz-dot-indent-width 4)

;; yaml
(load "highlight-indentation.el")
(add-hook 'yaml-mode-hook 'highlight-indentation-mode)

;; Navigation
;;;;;;;;;;;;;;;

;; In dired show directories first
(if (eq system-type 'gnu/linux)
    (setq dired-listing-switches "-alh --group-directories-first --time-style=long-iso"))

(require 'dired-collapse)
(require 'dired-subtree)

(add-hook 'dired-mode-hook
          (lambda ()
            (local-set-key (kbd "<SPC>") 'dired-subtree-toggle)
            (local-set-key (kbd "<tab>") 'dired-subtree-toggle)))

;; Filetree
(setq neo-window-fixed-size nil
      neo-window-width 40)

(global-set-key (kbd "C-c n t") 'neotree-toggle)

;; Enable which-key
(which-key-mode)

;; Move to beginning of line, or indentation
;; (defun my/bol-or-indent ()
;;   (interactive)
;;   (if (bolp)
;;       (back-to-indentation)
;;     (beginning-of-line)))

(defun my/point-at-indentation ()
  "Return non-nil if point is at indentation, nil otherwise."
  (= (save-excursion (back-to-indentation) (point)) (point)))

(defun my/bol-or-indent ()
  "Toggle between beginning of line and point of indentation."
  (interactive)
  (if (my/point-at-indentation)
      (beginning-of-line)
    (back-to-indentation)))

(global-set-key (kbd "C-a") 'my/bol-or-indent)


;; keystore
;;;;;;;;;;;;;

(require 'keystore-mode)


;; org-mode
;;;;;;;;;;;;;

(setq org-startup-folded nil)

(setq my-scratch-file (format-time-string "~/Projects/Org/GTD/scratch-%Y.org"))
(setq org-directory "~/Projects/Org")
(setq org-default-notes-file "~/Projects/Org/GTD/notes.org")
(setq org-capture-templates
      (quote (("a" "ActionItem"    entry (file+headline "GTD/notes.org" "Action Items") "* AI: %? :AI:\n%U\nDEADLINE: %^t\n%a\n%i\n")
              ("n" "Note"          entry (file+headline "GTD/notes.org" "Notes")        "* NOTE %? :NOTE:\n%U\n%a\n%i\n")
              ("t" "Task"          entry (file+headline "GTD/notes.org" "Tasks")        "* TASK %? :TASK:\n%U\nDEADLINE: %^t\n%a\n%i\n")
              ("s" "Scratch"       plain (file+olp+datetree my-scratch-file)            "**** %i%?\n" :time-prompt t :unnarrowed t :empty-lines 1)
              ("j" "Journal entry" plain (file+olp+datetree "GTD/journal.org")          "**** %i%?\n" :time-prompt t :unnarrowed t :empty-lines 1)
              ("m" "Meeting"       entry (file+headline "GTD/notes.org" "Meetings")     "* MEETING with %? :MEETING:\nSCHEDULED: %^T\n%U")
              ;; ("r" "Respond"       entry (file+headline "GTD/notes.org" "Respond to")   "* Respond to %:from on %:subject\nSCHEDULED: %t\n%U\n%a\n")
              ("x" "Question"      entry (file+headline "GTD/notes.org" "Questions")    "* QUESTION %? :QUESTION:\n%U\n%a\n"))))


;; Unique buffer names
;;;;;;;;;;;;;;;;;;;;;;;;

;; Prevent identical buffer names when files have identical names
;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Uniquify.html
(require 'uniquify)
(setq uniquify-buffer-name-style 'forward)
;; from prelude: https://github.com/bbatsov/prelude
(setq uniquify-separator "/")
(setq uniquify-after-kill-buffer-p t)    ; rename after killing uniquified
(setq uniquify-ignore-buffers-re "^\\*") ; don't muck with special buffers


;; Lisp settings
;;;;;;;;;;;;;;;;;;

;; Paredit
;; (autoload 'enable-paredit-mode "paredit"
;;   "Turn on pseudo-structural editing of Lisp code." t)

;; Common Lisp
(setq inferior-lisp-program "sbcl")

(add-hook 'emacs-lisp-mode-hook       #'eldoc-mode)
(add-hook 'lisp-interaction-mode-hook #'eldoc-mode)
(add-hook 'ielm-mode-hook             #'eldoc-mode)

(add-hook 'emacs-lisp-mode-hook       #'lispy-mode)
(add-hook 'lisp-interaction-mode-hook #'lispy-mode)
(add-hook 'lisp-mode-hook             #'lispy-mode)
(add-hook 'scheme-mode-hook           #'lispy-mode)

(add-hook 'eval-expression-minibuffer-setup-hook #'lispy-mode)

;; Hy
;;(add-hook 'hy-mode-hook #'enable-paredit-mode)
;;(add-to-list 'auto-mode-alist '("\\.hy$" . hy-mode))


;; Magit
;;;;;;;;;;

(setq magit-log-margin '(t "%Y-%m-%d %H:%M " magit-log-margin-width t 22))
(setq magit-display-buffer-function #'magit-display-buffer-fullframe-status-v1)
(setq magit-log-arguments '("--graph" "--color" "--decorate" "-n256" "++order=date"))


;; Ivy, Counsel, Swiper
;;;;;;;;;;;;;;;;;;;;;;;;;

;; Disable ido completely
(ido-mode -1)

;; Enable ivy
(ivy-mode 1)

;; add ‘recentf-mode’ and bookmarks to ‘ivy-switch-buffer’.
(setq ivy-use-virtual-buffers t)
(setq ivy-height 24)  ;; number of result lines to display
(setq enable-recursive-minibuffers t)
(setq ivy-use-selectable-prompt t)
(setq ivy-count-format "[%d/%d] ")
(setq ivy-display-style 'fancy)

;; Change behavior of ivy occur
;; See: https://oremacs.com/2017/11/18/dired-occur/
(ivy-set-occur 'ivy-switch-buffer 'ivy-switch-buffer-occur)

;; Support functions
(defun ivy-switch-buffer-occur ()
  "Occur function for `ivy-switch-buffer' using `ibuffer'."
  (ibuffer nil (buffer-name)
           (list (cons 'name ivy--old-re))))


;; Counsel
(setq counsel-rg-base-command
      "rg -S -M 120 --hidden --no-heading --line-number --color never %s .")


;; Swiper
;; Advise swiper to recenter on exit
(defun my/swiper-recenter (&rest args)
  "recenter display after swiper"
  (recenter))

(advice-add 'swiper :after #'my/swiper-recenter)


;; eshell
;;;;;;;;;;;

(add-hook 'eshell-mode-hook
          (lambda ()
            (add-to-list 'eshell-visual-commands "ssh")
            (add-to-list 'eshell-visual-commands "tail")
            (add-to-list 'eshell-visual-commands "htop")
            (add-to-list 'eshell-visual-commands "vim")
            (add-to-list 'eshell-visual-options '("git" "--help"))
            (add-to-list 'eshell-visual-subcommands'("git" "log" "diff" "show"))))

;; (setq eshell-prompt-regexp "^[^#$\n]*[#$] "
;;       eshell-prompt-function
;;       (lambda ()
;;         (concat "[" (user-login-name) "@" (system-name) " "
;;                 (if (string= (eshell/pwd) (getenv "HOME"))
;;                     "~" (eshell/basename (eshell/pwd)))
;;                 "]"
;;                 (if (= (user-uid) 0) "# " "$ "))))

;; shell-pop
(custom-set-variables
 ;; '(shell-pop-term-shell "/bin/bash")
 '(shell-pop-term-shell my-favorite-shell)
 ;; '(shell-pop-shell-type (quote ("shell" "*shell*" (lambda () (shell)))))
 '(shell-pop-shell-type (quote ("eshell" "*eshell*" (lambda () (eshell)))))
 ;; '(shell-pop-shell-type (quote ("ansi-term" "*ansi-term*" (lambda nil (ansi-term shell-pop-term-shell)))))
 ;; '(shell-pop-default-directory "/Users/kyagi/git")
 '(shell-pop-autocd-to-working-dir nil)
 '(shell-pop-window-size 40)
 '(shell-pop-full-span t)
 '(shell-pop-window-position "bottom"))


;; Diff settings
;;;;;;;;;;;;;;;;;;

(setq ediff-split-window-function 'split-window-horizontally
      ediff-window-setup-function 'ediff-setup-windows-plain)

;; Use Emacs as diff-tool: https://www.emacswiki.org/emacs/EdiffMode
;; Usage: emacs -ediff file1 file2
(defun command-line-ediff (switch)
  (let ((file1 (pop command-line-args-left))
        (file2 (pop command-line-args-left)))
    (ediff-files file1 file2)))

(add-to-list 'command-switch-alist '("-ediff" . command-line-ediff))

;; Copy changes from buffer A and B to C.
(defun my/ediff-copy-both-to-C ()
  (interactive)
  (ediff-copy-diff ediff-current-difference nil 'C nil
                   (concat
                    (ediff-get-region-contents ediff-current-difference 'A ediff-control-buffer)
                    (ediff-get-region-contents ediff-current-difference 'B ediff-control-buffer))))

(defun my/add-d-to-ediff-mode-map () (define-key ediff-mode-map "d" 'my/ediff-copy-both-to-C))
(add-hook 'ediff-keymap-setup-hook 'my/add-d-to-ediff-mode-map)


;; Java decompiler
;;;;;;;;;;;;;;;;;;;;

(customize-set-variable 'jdecomp-decompiler-type 'cfr)
(customize-set-variable 'jdecomp-decompiler-paths
                        '((cfr . "~/bin/cfr.jar")
                          (fernflower . "/opt/idea-IU/plugins/java-decompiler/lib/java-decompiler.jar")
                          (procyon . "/tmp/procyon-decompiler-0.5.30.jar")))
(add-to-list 'auto-mode-alist '("\\.class$" . jdecoomp-mode))
(jdecomp-mode nil)


;; UI
;;;;;;;

;; Cursor
(if (string= "root" (getenv "USER"))
    (progn
      (set-cursor-color "orange")
      (blink-cursor-mode t))
  (progn
    ;; (set-cursor-color "#ffe7a6")
    (blink-cursor-mode -1)))

;; (when (display-graphic-p)
;;   (global-hl-line-mode t))

;; Keep syntax highlighting
(set-face-foreground 'highlight nil)

;; Set font attributes depending on the OS
;; Note: List fonts using 'fc-list' in Linux
(cond
 ((eq system-type 'gnu/linux)
  ;; (set-face-attribute 'default nil :height 100)
  ;; (set-face-attribute 'default nil :font "Source Code Pro" :height 105)
  ;; (set-face-attribute 'default nil :font "DejaVu Sans Mono" :height 100)
  ;; (set-face-attribute 'default nil :font "Noto Sans Mono" :height 100)

  (add-to-list 'default-frame-alist '(font . "Roboto Mono 10"))
  (add-to-list 'default-frame-alist '(line-spacing . 0.0)))
 ((eq system-type 'windows-nt)
  (set-face-attribute 'default nil :family "Consolas" :height 105)))

;; Set the width and height at start
;; (setq initial-frame-alist '((top . 0) (left . 400) (width . 136) (height . 62)))
(setq initial-frame-alist '((width . 137) (height . 63)))

(when (fboundp 'tool-bar-mode)
  (tool-bar-mode -1))

;; Enable scrollbars
(setq scroll-bar-mode 1)
(scroll-bar-mode)


;; Editing
;;;;;;;;;;;;

;; Don't save file status
(setq desktop-save-mode nil)

;; Put all backups in ~/.emacs.d/backups
;; http://www.gnu.org/software/emacs/manual/html_node/elisp/Backup-Files.html
(setq backup-directory-alist `(("." . ,(concat user-emacs-directory "backups"))))
(setq auto-save-default nil)

;; (setq electric-indent-mode nil)
(setq electric-indent-mode t)

;; Automatically replace selection
(delete-selection-mode 1)

;; Don't highlight trailing whitespaces
(custom-set-variables
 '(show-trailing-whitespace nil))

;; Highlight symbols
(require 'highlight-symbol)
(setq highlight-symbol-idle-delay 0.4)
(add-hook 'prog-mode-hook #'highlight-symbol-mode)

;; Modes
(add-to-list 'auto-mode-alist '("\\.groovy\\'" . groovy-mode))

;; (require 'dockerfile-mode)
;; (add-to-list 'auto-mode-alist '("Dockerfile\\'" . dockerfile-mode))

;; Ripgrep
(require 'rg)


;; Functions
;;;;;;;;;;;;;;

;;;  Fix stuff
(defun my/fix-problems ()
  (interactive)
  (blink-cursor-mode -1)
  ;; (set-cursor-color "gold")
  )

;; Duplicate line under cursor (from EmacsWiki)
(defun my/duplicate-current-line (&optional n)
  "Duplicate current line, make more than 1 copy given a numeric argument."
  (interactive "p")
  (save-excursion
    (let ((nb (or n 1))
          (current-line (thing-at-point 'line)))
      ;; when on last line, insert a newline first
      (when (or (= 1 (forward-line 1)) (eq (point) (point-max)))
        (newline))
      ;; now insert as many time as requested
      (while (> n 0)
        (insert current-line)
        (cl-decf n)))))

;; Move lines
(defun my/move-line-up ()
  (interactive)
  (transpose-lines 1)
  (previous-line 2))

(defun my/move-line-down ()
  (interactive)
  (next-line 1)
  (transpose-lines 1)
  (previous-line 1))

;; Convert between EDN/JSON/YAML using a babashka tool
(defun my/ejy-convert (out)
  (shell-command-on-region
   (region-beginning) (region-end)
   (format "ejy-convert.bb %s " out)
   nil "REPLACE" nil t))
(defun my/ejy-convert-to-edn  () (interactive) (my/ejy-convert "edn"))
(defun my/ejy-convert-to-json () (interactive) (my/ejy-convert "json"))
(defun my/ejy-convert-to-yaml () (interactive) (my/ejy-convert "yaml"))

;; Reformat code
(defun my/reformat-xml ()
  "Re-Format XML using sgml-mode pretty-printer"
  (interactive)
  (save-excursion
    (sgml-pretty-print (point-min) (point-max))
    (indent-region (point-min) (point-max))))

(defun my/xmllint-format ()
  "Reformat XML using the xmllint tool"
  (interactive)
  (shell-command-on-region
   (point-min)
   (point-max)
   "xmllint --format -"
   (current-buffer) ;; output buffer
   t ;; replace?
   "*xmllint Error Buffer*" ;; name of the error buffer
   t ;; show error buffer?
   ))

(defun my/reformat-json ()
  "Re-Format JSON using external tool 'jq'"
  (interactive)
  (save-excursion
    (shell-command-on-region
     (point-min) (point-max)
     "jq ." (buffer-name) t)))

(defun my/sudo-edit (&optional arg)
  "Edit currently visited file as root.

     With a prefix ARG prompt for a file to visit.
     Will also prompt for a file to visit if current
     buffer is not visiting a file."
  (interactive "P")
  (if (or arg (not buffer-file-name))
      (find-file (concat "/sudo:root@localhost:"
                         (ido-read-file-name "Find file(as root): ")))
    (find-alternate-file (concat "/sudo:root@localhost:" buffer-file-name))))

;; Dired
(defvar my-dired-show-hidden t)
(defvar my-dired-hide-hidden-switches "-lh --group-directories-first --time-style=long-iso")

(define-key dired-mode-map (kbd "C-c g i") 'dired-git-info-mode)
(define-key dired-mode-map (kbd ")") 'dired-git-info-mode)

(defun my/dired-toggle-hidden-files-view ()
  (interactive)
  (setq my-dired-show-hidden (not my-dired-show-hidden))
  (if (eq my-dired-show-hidden t)
      (setq dired-actual-switches dired-listing-switches)
    (setq dired-actual-switches my-dired-hide-hidden-switches))
  ;; (message "Show hidden files: %s" (if (eq my-dired-show-hidden t) "t" "nil"))
  (revert-buffer))

(defun my/open-in-external-app (&optional @fname)
  "Open the current file or dired marked files in external app.
      The app is chosen from your environment via xdg-open.
      When called in emacs lisp, if FNAME is given, open that."
  (interactive)
  (let* (($file-list
          (if @fname
              (progn (list @fname))
            (if (string-equal major-mode "dired-mode")
                (dired-get-marked-files)
              (list (buffer-file-name)))))
         ($do-it-p (if (<= (length $file-list) 5)
                       t
                     (y-or-n-p "Open more than 5 files? "))))
    (when $do-it-p
      (mapc
       (lambda ($fpath) (let ((process-connection-type nil))
                     (start-process "" nil "xdg-open" $fpath)))
       $file-list))))

(add-hook 'dired-mode-hook
          (lambda ()
            (local-set-key (kbd "C-c C-o") 'my/open-in-external-app)
            (local-set-key (kbd "h") 'my/dired-toggle-hidden-files-view)))


;; x509
;;;;;;;;;

;; Show the SHA256 fingerprint as well
;; (setq x509-x509-default-arg "x509 -text -noout -nameopt utf8 -nameopt multiline -fingerprint -sha256")

;; Alias
;;;;;;;;;;

(defalias 'esh 'eshell)
(defalias 'sl 'sort-lines)
(defalias 'lp 'list-packages)
(defalias 'prc 'package-refresh-contents)

;; Certificate stuff
(defalias 'kv 'keystore-visit)
(defalias 'vc 'x509-viewcert)

;; Keys
;;;;;;;;;

;; Misc
(global-set-key [(M-S-up)] 'my/move-line-up)
(global-set-key [(M-S-down)] 'my/move-line-down)

(global-set-key [home] 'my/bol-or-indent)
(global-set-key [end] 'end-of-line)

(global-set-key (kbd "M-g M-g") 'goto-line)

(global-set-key (kbd "C-/") 'comment-region)
(global-set-key (kbd "C-;") 'my/toggle-comment-on-line)

(global-set-key (kbd "C-c d") 'my/duplicate-current-line)
(global-set-key (kbd "C-c r") 'my/rename-current-buffer-file)
(global-set-key (kbd "C-c f") 'find-file-at-point)

(global-set-key (kbd "C-x C-r") #'my/sudo-edit)

;; Searching
(global-set-key (kbd "C-S-s") 'isearch-forward-regexp)
(global-set-key (kbd "C-S-r") 'isearch-backward-regexp)

;; Windows
;; (global-set-key (kbd "M-o") 'other-window)  ;; Was originally set to something useless
(global-set-key (kbd "C-c C-<left>")  'windmove-left)
(global-set-key (kbd "C-c C-<right>") 'windmove-right)
(global-set-key (kbd "C-c C-<up>")    'windmove-up)
(global-set-key (kbd "C-c C-<down>")  'windmove-down)

;; Switch buffers
(global-set-key (kbd "C-o") 'mode-line-other-buffer)

;; I never use overwrite mode
(global-set-key (kbd "<insert>") 'scroll-lock-mode)
(global-set-key (kbd "<Scroll_Lock>") 'scroll-lock-mode)

;; Simulate some hydra menus
(global-set-key (kbd "C-c m m a") 'auto-fill-mode)
(global-set-key (kbd "C-c m m o") 'org-mode)

;; Highlight symbols
(global-set-key (kbd "M-s n") 'highlight-symbol-next)
(global-set-key (kbd "M-s p") 'highlight-symbol-prev)
(global-set-key (kbd "M-s h ,") 'highlight-symbol)
(global-set-key (kbd "M-s h q") 'highlight-symbol-query-replace)

;; Magit
(global-set-key (kbd "C-c g a") 'vc-annotate)
(global-set-key (kbd "C-c g b") 'magit-blame)
(global-set-key (kbd "C-c g d") 'magit-dispatch-popup)
(global-set-key (kbd "C-c g f") 'magit-file-popup)
(global-set-key (kbd "C-c g g") 'magit-status)
(global-set-key (kbd "C-c g h") 'magit-log-buffer-file)
(global-set-key (kbd "C-c g r") 'magit-list-repositories)

(global-set-key (kbd "C-x C-b") 'ibuffer)

;; Docker
(global-set-key (kbd "C-c D") #'docker)

;; Shell pop
(global-set-key (kbd "C-x t") 'shell-pop)

;; Swiper, Ivy, Counsel
;;;;;;;;;;;;;;;;;;;;;;;;;

;; Swiper
(global-set-key (kbd "C-s") 'swiper)
(global-set-key (kbd "C-c s") 'isearch-forward)
(define-key isearch-mode-map (kbd "M-i") 'swiper-from-isearch) ;; Does not work :(

;; Ivy & Counsel functions
;; No initial '^' for "M-x"
(global-set-key (kbd "M-x") (lambda () (interactive) (counsel-M-x "")))
(global-set-key (kbd "M-y") 'counsel-yank-pop)

(global-set-key (kbd "C-x b") 'ivy-switch-buffer)
(global-set-key (kbd "C-x B") 'counsel-recentf)
(global-set-key (kbd "C-x C-f") 'counsel-find-file)

(global-set-key (kbd "C-c i") 'counsel-imenu)
(global-set-key (kbd "C-c C-f") 'counsel-git)
(global-set-key (kbd "C-c C-g") 'counsel-rg)
(global-set-key (kbd "C-c C-S-g") 'counsel-projectile-rg)  ;; Also "C-c p s s"

;; Org-mode functionality everywhere
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(global-set-key (kbd "C-c |") #'org-table-create-or-convert-from-region)
(global-set-key (kbd "C-c -") #'org-table-insert-hline)
(global-set-key (kbd "C-c c") 'org-capture)  ;; To take notes

;; Multiple Cursors
;;;;;;;;;;;;;;;;;;;;;

;; Add a cursor to each line of an active region
;; (global-set-key (kbd "C-? C-?") 'mc/edit-lines)

;; Add multiple cursors
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/unmark-next-like-this)
(global-set-key (kbd "C-M->") 'mc/skip-to-next-like-this)
(global-set-key (kbd "C-M-<") 'mc/skip-to-previous-like-this)
(global-set-key (kbd "C-S-<mouse-1>") 'mc/add-cursor-on-click)
(global-set-key (kbd "C-c C->") 'mc/mark-all-like-this)

;; Tools
;;;;;;;;;;

(global-set-key (kbd "C-c t") 'leo-translate-word)

;; Function keys
;;;;;;;;;;;;;;;;;;

(global-set-key [f1] 'describe-mode)
;; (global-set-key [f2] 'save-buffer)
;; (global-set-key [(control f2)] 'my/remove-ws-save-buffer)
(global-set-key [f3] 'my/clean-buffer)
(global-set-key [f4] 'calc)
(global-set-key [f5] 'revert-buffer)
(global-set-key [f6] 'start-kbd-macro)
(global-set-key [f7] 'end-kbd-macro)
(global-set-key [f8] 'call-last-kbd-macro)
(global-set-key [f9] 'display-line-numbers-mode)
(global-set-key [(shift f9)] 'scroll-bar-mode)
(global-set-key [f10] 'toggle-truncate-lines)
(global-set-key [(shift f10)] 'read-only-mode)

(when (display-graphic-p)
  ;; Store positions in register
  (global-set-key (kbd "C-M-0") "\C-xr 0")
  (global-set-key (kbd "C-M-9") "\C-xr 9")
  (global-set-key (kbd "C-M-8") "\C-xr 8")
  (global-set-key (kbd "C-M-7") "\C-xr 7")
  (global-set-key (kbd "C-M-6") "\C-xr 6")

  (global-set-key (kbd "M-0") "\C-xrj0\C-l")
  (global-set-key (kbd "M-9") "\C-xrj9\C-l")
  (global-set-key (kbd "M-8") "\C-xrj8\C-l")
  (global-set-key (kbd "M-7") "\C-xrj7\C-l")
  (global-set-key (kbd "M-6") "\C-xrj6\C-l")

  ;; Store regions in register
  (global-set-key (kbd "C-M-1") "\C-xrs1")
  (global-set-key (kbd "C-M-2") "\C-xrs2")
  (global-set-key (kbd "C-M-3") "\C-xrs3")
  (global-set-key (kbd "C-M-4") "\C-xrs4")
  (global-set-key (kbd "C-M-5") "\C-xrs5")

  (global-set-key (kbd "M-1") "\C-xrg1")
  (global-set-key (kbd "M-2") "\C-xrg2")
  (global-set-key (kbd "M-3") "\C-xrg3")
  (global-set-key (kbd "M-4") "\C-xrg4")
  (global-set-key (kbd "M-5") "\C-xrg5"))


;; Misc
;;;;;;;;;

;; Set the window title
(setq frame-title-format my-title-format)

;; Stupid desktop
(setq desktop-save-mode nil)

;; EOF
