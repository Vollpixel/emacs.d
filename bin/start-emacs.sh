#!/bin/bash

################################################################
# Script to start Emacs with different configurations
################################################################

EMACS=emacs
# EMACS=/opt/my-emacs-29.1/bin/emacs

usage()
{
  echo
  echo "$0 min|<env> [<file>] | [-h]"
  echo
}

if [ "$1" = "-h" ]
then
  usage
  exit 0
fi

if [ "x$1" = "x" ]
then
  $EMACS -q -l ~/.emacs.d/init-min.el
else
  _config=$1
  shift

  case $_config in
    min)
      $EMACS -q -l ~/.emacs.d/init-min.el "$@"
      ;;
    *)
      # Strange behaviour when not called with -q -l ...
      export EMACS_ENV=$_config && $EMACS -q -l ~/.emacs.d/init.el "$@"
      ;;
  esac
fi
