#!/bin/sh

version="2.1"

help() {
    echo "Usage: $(basename $0) [-h] [-c]

    Run the Emacs calculator.

        -h    Show this help
        -c    Enable CUA mode (Emacs default, less novice-friendly)"
    exit
}

calcElisp="(progn
  (when (display-graphic-p)
    (tool-bar-mode -1)
    (scroll-bar-mode -1))
  ;;(menu-bar-mode -1)
  (blink-cursor-mode -1)

  (setq
     select-enable-primary t
     help-window-select t)

  (cua-mode $cua)
  (when cua-mode
     (defun calc-mode? ()
        (eq major-mode (quote calc-mode)))

     (define-advice cua-paste (:before-while (arg))
       (and (calc-mode?) (calc-yank arg))))

  (full-calc)

  (define-key calc-mode-map [?q]
    (quote save-buffers-kill-terminal))

  (define-advice calc-full-help (:after nil)
    (switch-to-buffer-other-window \"*Help*\")
    (message \"Press q to go back to calc\")))"

config="-q -l ~/.emacs.d/init-app.el"
cua="nil"

while getopts "hc" name; do
    case "$name" in
        h) help ;;
        c) cua="t" ;;
        *) help ;;
    esac
done

#emacs -Q -nw --eval="$calcElisp"
emacs $config -nw --eval="$calcElisp"
