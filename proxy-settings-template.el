;;;;
;; ~/emacs.d/proxy-settings-template.el
;;
;; HTTPS proxy settings for Emacs.
;;
;; Make a copy of this file to ~/.emacs.d/proxy-settings.el
;; and configure your proxy in it.
;;;;

;; Web-proxy
;; (setq url-proxy-services
;;       '(("no_proxy" . "^\\(localhost\\|10.*\\)")
;;         ("http" . "proxy.com:8080")
;;         ("https" . "proxy.com:8080")))
