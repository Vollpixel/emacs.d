;;;;
;; ~/emacs.d/local-settings-template.el
;;
;; Local settings for Emacs. This includes all settings
;; that can differ on systems.
;;
;; Make a copy of this file to ~/.emacs.d/local-settings.el
;; and apply your settings.
;;;;

;; Name and e-mail
(setq user-full-name "My Name"
      user-mail-address "my.name@mail.com"
      mu4e-reply-to-address "my.name@mail.com")

;; Add texlive path
(when (eq system-type 'gnu/linux)
  (let ((latex-path "/opt/texlive/2024/bin/x86_64-linux"))
    (add-to-list 'exec-path latex-path t)
    (setenv "PATH" (concat (getenv "PATH") ":" latex-path))))

;; Define the external web-browser
;; (cond ((eq system-type 'gnu/linux)
;;        (setq browse-url-browser-function 'browse-url-generic
;;              browse-url-generic-program "~/bin/launchbrowser"))
;;       ((eq system-type 'windows-nt)
;;        (setq browse-url-browser-function 'browse-url-generic
;;              browse-url-generic-program "C:/Users/winuser/AppData/Local/Vivaldi/Application/vivaldi.exe")))

;; List git repositories
(setq magit-repository-directories
      '(("/data/git-src" . 1)
        ("~/Projects" . 2)))

;; List of favorite external systems
;; (setq my-favorite-remote-machines
;;       `(("remsys1" . ,(list :username "root" :ip "system1.domain.com" :port "22"))
;;         ("remsys2" . ,(list :username "user" :ip "10.10.10.10" :port "22"))))
