;;;;
;;            micro-init.el
;; Minimal configuration for faster load
;;;;

;; Define, which configuration this is
(setq my-use-config "bare")

;; Make it possible to identify the editor
(setq my-title-format (upcase (concat "[" my-use-config "] - %f")))

;; Save desktop files in the session directory
(defvar my-session-dir (concat (getenv "HOME") "/.emacs.d/.s-" my-use-config "/")
  "Sub-directory under ~/.emacs.d for environment specific configuration files.")
(unless (file-exists-p my-session-dir)
  (make-directory my-session-dir))

;; HTTPS proxy settings that differ between systems.
(let ((my-proxy-settings-file "~/.emacs.d/proxy-settings.el"))
  (when (file-exists-p my-proxy-settings-file)
    (load-file my-proxy-settings-file)))

;; Load local settings that may differ between systems
(let ((my-local-settings-file "~/.emacs.d/local-settings.el"))
  (when (file-exists-p my-local-settings-file)
    (load-file my-local-settings-file)))


;; Packages
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Define package repositories
(require 'package)

(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)

;; Load and activate emacs packages
(package-initialize)

;; Download the ELPA archive description if needed
(when (not package-archive-contents)
  (package-refresh-contents))

;; Install wanted packages. These can also be installed with M-x package-install
(defvar my-packages
  '(
    ;; Modules
    lispy

    swiper
    counsel

    ;; Tools
    which-key

    ;; UI
    smart-mode-line

    ;; Modes
    json-mode
    yaml-mode

    ;; Themes
    ;; arjen-grey-theme
    ;; afternoon-theme
    ;; flatland-theme
    ;; spacegray-theme
    ))

;; Install packages, that are not already installed
(dolist (p my-packages)
  (when (not (package-installed-p p))
    (package-install p)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Customization files
;;;;;;;;;;;;;;;;;;;;;;;;

(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")
(add-to-list 'load-path "~/.emacs.d/themes")


;; Theme
;;;;;;;;;;

;; (if (display-graphic-p)
;;     (load-theme 'afternoon t))

(load-theme 'nordisch t)


;; Modeline
;;;;;;;;;;;;;

(setq sml/no-confirm-load-theme t)
;;(setq sml/theme 'powerline)
(setq sml/theme 'respectful)
;;(setq sml/theme 'dark)

;; Enable smart mode line
(sml/setup)


;; Custom variables
;;;;;;;;;;;;;;;;;;;;;

;; Define my favorite shell
(defvar my-favorite-shell "/bin/bash" "My favorite shell.")

;; Shell to use in docker or term
(setq shell-file-name my-favorite-shell)

(setq custom-file "~/.emacs.d/settings.el")

(if (file-exists-p custom-file)
    (load custom-file))

;; Changes all yes/no questions to y/n type
(fset 'yes-or-no-p 'y-or-n-p)

;; Use UTF-8 as default encoding
(prefer-coding-system 'utf-8)
(setq-default buffer-file-coding-system 'utf-8-unix)

;; Jump into help window
(setq help-window-select t)

(setq make-backup-files nil)
(setq auto-save-default nil)

;; System PATH
(when (string-equal system-type "windows-nt")
  (setenv "PATH" (concat (getenv "PATH") ";"
                         "C:\\Program Files\\Git\\usr\\bin" ";"
                         "C:\\Users\\pestoldt.CORPDOM\\bin")))

(setq create-lockfiles nil)
;; Go straight to scratch buffer on startup
(setq inhibit-startup-message t)

;; NO BELL !!!
(setq ring-bell-function 'ignore)

;; Start scrolling slightly before end of window
(setq scroll-margin 1
      scroll-step 1
      scroll-conservatively 10000)

;; Newline at end of file
(setq require-final-newline t)

;; Enable commands to change case (C-x C-u, C-x C-l)
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'narrow-to-region 'disabled nil)

;; OS interaction
(setq x-select-enable-clipboard t
      x-select-enable-primary t
      save-interprogram-paste-before-kill t
      apropos-do-all t
      mouse-yank-at-point t)


;; Navigation
;;;;;;;;;;;;;;;

;; In dired show directories first
(if (eq system-type 'gnu/linux)
    (setq dired-listing-switches "-alh --group-directories-first"))

;; Enable which-key
(which-key-mode)


;; Indentation
;;;;;;;;;;;;;;;;

(setq tab-width 4)

;; Use Tab to Indent or Complete
(setq tab-always-indent 'complete)

;; Don't use hard tabs
(setq-default indent-tabs-mode nil)

;; default indentation
(setq-default sh-basic-offset 4)
(setq-default sh-indentation 4)

(add-hook 'java-mode-hook
          (lambda ()
            (setq c-basic-offset 4
                  tab-width 4)))

;; nXML Extrawurst
(setq nxml-child-indent 4
      nxml-attribute-indent 4)

;; graphviz-dot-mode
(setq graphviz-dot-indent-width 4)


;; Unique buffer names
;;;;;;;;;;;;;;;;;;;;;;;;

;; Prevent identical buffer names when files have identical names
;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Uniquify.html
(require 'uniquify)
(setq uniquify-buffer-name-style 'forward)
;; from prelude: https://github.com/bbatsov/prelude
(setq uniquify-separator "/")
(setq uniquify-after-kill-buffer-p t)    ; rename after killing uniquified
(setq uniquify-ignore-buffers-re "^\\*") ; don't muck with special buffers


;; Lisp settings
;;;;;;;;;;;;;;;;;;

;; (autoload 'enable-paredit-mode "paredit"
;;   "Turn on pseudo-structural editing of Lisp code." t)

(add-hook 'emacs-lisp-mode-hook       #'lispy-mode)
(add-hook 'eval-expression-minibuffer-setup-hook #'lispy-mode)
(add-hook 'lisp-mode-hook             #'lispy-mode)
(add-hook 'lisp-interaction-mode-hook #'lispy-mode)
(add-hook 'scheme-mode-hook           #'lispy-mode)


;; Common Lisp
(setq inferior-lisp-program "sbcl")
(setq slime-contribs '(slime-fancy))

(add-hook 'emacs-lisp-mode-hook 'turn-on-eldoc-mode)
(add-hook 'lisp-interaction-mode-hook 'turn-on-eldoc-mode)
(add-hook 'ielm-mode-hook 'turn-on-eldoc-mode)


;; Ivy, Counsel, Swiper
;;;;;;;;;;;;;;;;;;;;;;;;;

;; Disable ido completely
(ido-mode -1)

;; Enable ivy
(ivy-mode 1)

;; add ‘recentf-mode’ and bookmarks to ‘ivy-switch-buffer’.
(setq ivy-use-virtual-buffers t)
(setq ivy-height 24)  ;; number of result lines to display
(setq enable-recursive-minibuffers t)
(setq ivy-use-selectable-prompt t)
(setq ivy-count-format "[%d/%d] ")
(setq ivy-display-style 'fancy)

;; Change behavior of ivy occur
;; See: https://oremacs.com/2017/11/18/dired-occur/
(ivy-set-occur 'ivy-switch-buffer 'ivy-switch-buffer-occur)

;; Support functions
(defun ivy-switch-buffer-occur ()
  "Occur function for `ivy-switch-buffer' using `ibuffer'."
  (ibuffer nil (buffer-name)
           (list (cons 'name ivy--old-re))))

;; Counsel
(setq counsel-rg-base-command
      "rg -S -M 120 --hidden --no-heading --line-number --color never %s .")

;; Swiper
;; Advise swiper to recenter on exit
(defun my/swiper-recenter (&rest args)
  "recenter display after swiper"
  (recenter))

(advice-add 'swiper :after #'my/swiper-recenter)


;; eshell
;;;;;;;;;;;

(add-hook 'eshell-mode-hook
          (lambda ()
            (add-to-list 'eshell-visual-commands "ssh")
            (add-to-list 'eshell-visual-commands "tail")
            (add-to-list 'eshell-visual-commands "htop")
            (add-to-list 'eshell-visual-commands "vim")
            (add-to-list 'eshell-visual-options '("git" "--help"))
            (add-to-list 'eshell-visual-subcommands'("git" "log" "diff" "show"))))

;; ediff
(setq ediff-split-window-function 'split-window-vertically)

;; Use Emacs as diff-tool: https://www.emacswiki.org/emacs/EdiffMode
;; Usage: emacs -ediff file1 file2
(defun command-line-ediff (switch)
  (let ((file1 (pop command-line-args-left))
        (file2 (pop command-line-args-left)))
    (ediff-files file1 file2)))

(add-to-list 'command-switch-alist '("-ediff" . command-line-ediff))


;; UI
;;;;;;;

;; Cursor
(blink-cursor-mode -1)
(set-cursor-color "#ffe7a6")

;; (global-hl-line-mode 1)
;; (set-face-background 'hl-line "#36393d")
;; Keep syntax highlighting
(set-face-foreground 'highlight nil)

;; Set font attributes depending on the OS
(cond
 ((string-equal system-type "gnu/linux")
  ;;(set-face-attribute 'default nil :height 100)
  (set-face-attribute 'default nil :font "Source Code Pro" :height 105))
 ((string-equal system-type "windows-nt")
  (set-face-attribute 'default nil :family "Consolas" :height 105)))

;; Set the width and height at start
(setq initial-frame-alist '((top . 0) (left . 400) (width . 140) (height . 68)))

(when (fboundp 'tool-bar-mode)
  (tool-bar-mode -1))

;; Enable scrollbars
(setq scroll-bar-mode 1)
(scroll-bar-mode)


;; Editing
;;;;;;;;;;;;

;; Don't save file status
(setq desktop-save-mode nil)

;; Put all backups in ~/.emacs.d/backups
;; http://www.gnu.org/software/emacs/manual/html_node/elisp/Backup-Files.html
(setq backup-directory-alist `(("." . ,(concat user-emacs-directory "backups"))))
(setq auto-save-default nil)

;; (setq electric-indent-mode nil)
(setq electric-indent-mode t)

;; Automatically replace selection
(delete-selection-mode 1)

;; Don't highlight trailing whitespaces
(custom-set-variables
 '(show-trailing-whitespace nil))

;; Highlight symbols
(require 'highlight-symbol)


;; Keys
;;;;;;;;;

;; Misc
(global-set-key [(M-S-up)] 'move-text-up)
(global-set-key [(M-S-down)] 'move-text-down)

(global-set-key [end] 'end-of-line)

(global-set-key (kbd "C-/") 'comment-region)
(global-set-key (kbd "C-c f") 'find-file-at-point)

;; imenu
(global-set-key (kbd "C-c i") 'counsel-imenu)

;; Searching
(global-set-key (kbd "C-S-s") 'isearch-forward-regexp)
(global-set-key (kbd "C-S-r") 'isearch-backward-regexp)

;; Windows
(global-set-key (kbd "C-c C-<left>")  'windmove-left)
(global-set-key (kbd "C-c C-<right>") 'windmove-right)
(global-set-key (kbd "C-c C-<up>")    'windmove-up)
(global-set-key (kbd "C-c C-<down>")  'windmove-down)

;; Switch buffers
(global-set-key (kbd "C-o") 'mode-line-other-buffer)

;; I never use overwrite mode
(global-set-key (kbd "<insert>") 'scroll-lock-mode)
(global-set-key (kbd "<Scroll_Lock>") 'scroll-lock-mode)

;; Open specific files
(global-set-key (kbd "C-c m") (lambda () (interactive) (find-file "~/Projects/Org/menu.org")))

;; Highlight symbols
(global-set-key (kbd "C-, h") 'highlight-symbol)
(global-set-key (kbd "C-, n") 'highlight-symbol-next)
(global-set-key (kbd "C-, p") 'highlight-symbol-prev)
(global-set-key (kbd "C-, r") 'highlight-symbol-query-replace)

(global-set-key (kbd "C-x C-b") 'ibuffer)

;; Swiper, Ivy, Counsel
;;;;;;;;;;;;;;;;;;;;;;;;;

;; Swiper
(global-set-key (kbd "C-s") 'swiper)
(global-set-key (kbd "C-c s") 'isearch-forward)
(define-key isearch-mode-map (kbd "M-i") 'swiper-from-isearch) ;; Does not work :(

;; Ivy & Counsel functions
;; No initial '^' for "M-x"
(global-set-key (kbd "M-x") (lambda () (interactive) (counsel-M-x "")))
(global-set-key (kbd "M-y") 'counsel-yank-pop)

(global-set-key (kbd "C-x b") 'ivy-switch-buffer)
(global-set-key (kbd "C-x B") 'counsel-recentf)
(global-set-key (kbd "C-x C-f") 'counsel-find-file)
(global-set-key (kbd "C-c C-f") 'counsel-git)


;; Misc
;;;;;;;;;

;; Set the window title
(setq frame-title-format my-title-format)

;; EOF
